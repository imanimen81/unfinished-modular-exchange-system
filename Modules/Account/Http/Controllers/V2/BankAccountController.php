<?php

namespace Modules\Account\Http\Controllers\V2;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Account\Entities\BankAccount;
use Modules\Account\Http\Requests\AddCardRequest as AddCardRequest;

class BankAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function getCards(){
        try {
            $data = BankAccount::getAccounts();
            return response()->json([
                'data' => $data,
            ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    public function getShebaNumbers(){
        try {
            $user_id = auth()->user()->id;
            $data = BankAccount::getShebaAccounts($user_id);
            return response()->json([
                'data' => $data,
            ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    public function addCard(AddCardRequest $request){
        try {
            $user_id = auth()->user()->id;
                $data = $request->all();
                $data['user_id'] = $user_id;
                $addAccount = (new BankAccount)->addAccount($data);
                return response()->json([
                    'message' => 'Bank Account Added Successfully!',
                    'data' => $addAccount,
                ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' =>
                [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }


    public function addShebaNumber(Request $request){ // todo: match with UI
        try {
            $user_id = auth()->user()->id;
            $bankName = $request->get('bank_name');
            $sheba = $request->get('sheba_number');
            $account = BankAccount::query()->create([
                'user_id' => $user_id,
                'bank_name' => $bankName,
                'sheba_number' => $sheba
            ]);
            return response()->json([
                'message' => 'Sheba Added Successfully!',
                'data' => $account,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function updateAccount(Request $request){
        try {
            $data = $request->all();
            $data['user_id'] = auth()->user()->id;
            $updateAccount = (new BankAccount)->updateAccount($data);
            return response()->json([
                'message' => 'Bank Account Updated Successfully!',
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    public function removeAccount(Request $request){
        try {
            $data = $request->all();
            $data['user_id'] = auth()->user()->id;
            $removeAccount = BankAccount::removeCard($data['user_id'], $data['id']);
            return response()->json([
                'message' => 'Bank Account Removed Successfully!',
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    protected function getAccount(Request $request){
        return BankAccount::query()
            ->where('id', $request->get('account_id'))
            ->firstOrFail();
    }
}
