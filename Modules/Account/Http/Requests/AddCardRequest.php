<?php

namespace Modules\Account\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCardRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sheba_number' => 'nullable|string|max:50|unique:bank_accounts,sheba_number',
            'account_number' => 'required|string|max:50|unique:bank_accounts,account_number',
            'card_number' => 'required|string|max:50|unique:bank_accounts,card_number',
            'bank_name' => 'required|string|max:50',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
