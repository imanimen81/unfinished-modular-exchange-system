<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Account\Http\Controllers\V2\BankAccountController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v2/account/card'], function ($router){
    $router->get('/get-cards',[BankAccountController::class, 'getCards']);
    $router->get('/get-sheba',[BankAccountController::class, 'getShebaNumbers']);
    $router->post('/add-card', [BankAccountController::class, 'addCard']);
    $router->post('/add-sheba', [BankAccountController::class, 'addShebaNumber']);
    $router->post('/update-card', [BankAccountController::class, 'updateAccount']);
    $router->post('/remove-card', [BankAccountController::class, 'removeAccount']);
});
