<?php

namespace Modules\Account\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use HasFactory, SoftDeletes;


    protected $appends = ['status_label'];
    protected $table = 'bank_accounts';
    protected $fillable = [
        'full_name',
        'user_id',
        'bank_name',
        'card_number',
        'sheba_number',
        'account_number',
        'is_verified',
        'status',
        'reject_reason',
        'account_id',
    ];

    const STATUS = [
      0 => 'Pending',
      1 => 'Verified',
      2 => 'Rejected',
      3 => 'Deleted',
    ];
    const PENDING = 0;
    const VERIFIED = 1;
    const REJECTED = 2;
    const DELETED = 3;

    const isVerified = [
      1 => 'Verified',
      2 => 'Rejected'
    ];

    public function getStatusLabelAttribute()
    {
        if ($this->status == self::PENDING) {
            return 'pending';
        }
        if ($this->status == self::VERIFIED){
            return 'verified';
        }
        if($this->status == self::REJECTED){
            return 'rejected';
        }
        if ($this->status == self::DELETED){
            return 'deleted';
        }
    }


    protected static function newFactory()
    {
        return \Modules\Account\Database\factories\BankAcoountFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getAccounts(){
        $user = auth()->user();
        return $user->accounts;
    }

    public static function getShebaAccounts($user_id){
        return BankAccount::query()
            ->where('user_id', $user_id)
            ->select(['id','bank_name', 'sheba_number'])
            ->get();
    }

    public function addAccount($data)
    {
        return BankAccount::query()->create($data);
    }

    public function addAccountByShebaNumber($data){
        return BankAccount::query()->create($data);
    }

    public function updateAccount($data){
        $account = BankAccount::query()
            ->where('id', $data['id'])
            ->firstOrFail();
        $account->update($data);
        return $account;
    }

    public static function removeCard($user_id, $card_id){
        $account = BankAccount::query()
            ->where('user_id', $user_id)
            ->where('id', $card_id)
            ->firstOrFail();
        $account->update(['status' => self::DELETED]);
        $account->delete();
    }
}
