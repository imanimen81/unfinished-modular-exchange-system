<?php

namespace Modules\Coin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Trade\Entities\Trade;
use Modules\Wallet\Entities\Wallet;

class Coin extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'key',
        'rial_price',
        'usd_price',
        'status',
        'sell_price_rial',
        'sell_price_usd',
        'buy_price_rial',
        'buy_price_usd',
        'rial_rate',
        'usd_rate',
        'peer',
        'wage',
        'icon',
    ];

    protected $appends = ['icon_url'];
    public function getIconUrlAttribute(): string
    {
        return 'http://kamex.dyneemadev.com/'.$this->icon;
    }

    protected $table = 'coins';

    protected static function newFactory()
    {
        return \Modules\Coin\Database\factories\CoinFactory::new();
    }

    public function trades(){
        return $this->hasMany(Trade::class);
    }

    public function wallets(){
        return $this->hasMany(Wallet::class);
    }
}
