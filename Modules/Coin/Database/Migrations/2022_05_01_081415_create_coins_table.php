<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('key')->nullable();
            $table->double('rial_price')->nullable();
            $table->double('usd_price')->nullable();
            $table->double('buy_price_rial')->nullable();
            $table->double('sell_price_rial')->nullable();
            $table->double('buy_price_usd')->nullable();
            $table->double('sell_price_usd')->nullable();
            $table->integer('peer')->nullable();
            $table->integer('status')->nullable();
            $table->string('icon')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
