<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Coin\Http\Controllers\V2\CoinActionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v2/coin'], function ($router) {
    $router->get('/coins', [CoinActionController::class, 'getCoins']);
    $router->get('/coins/update-price', [CoinActionController::class, 'livePrice']);
});
