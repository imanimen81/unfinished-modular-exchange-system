<?php

namespace Modules\Coin\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Coin\Entities\Coin;
use Symfony\Component\Console\Output\ConsoleOutput;

class CoinPrice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const REMOTE_URL = 'https://api.binance.com/api/v3/ticker/price';

    public $name;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $conName = $this->name;
            $coinName = $conName.'USDT';
            $client =  new Client();
            $response = $client->request('GET', 'https://api.binance.com/api/v3/ticker/price?symbol='.$coinName);
            $arr = json_decode($response->getBody()->getContents(), true);
            $responseCode = $response->getStatusCode();
            $c = Coin::query()->where('name', $conName);
            $rial = $arr['price'] * 30500.000;
            $c->update(['usd_price' => $arr['price']]);
            $c->update(['rial_price' => $rial]);

            if ($responseCode == 200) {
                $log = new ConsoleOutput();
                $log->writeln('Coin Price Updated. Coin: '.$this->name);
            } else {
                $log = new ConsoleOutput();
                $log->writeln('Coin Price Update Failed. Coin: '.$this->name);
            }
        } catch (\Exception $e) {
            $log = new ConsoleOutput();
            $log->writeln('Coin Price Update Failed. Error: '.$e->getMessage());
        }
    }
}
