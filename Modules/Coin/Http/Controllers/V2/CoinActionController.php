<?php

namespace Modules\Coin\Http\Controllers\V2;

use GuzzleHttp\Client;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Coin\Entities\Coin;

class CoinActionController extends Controller
{

    // get coins
    public function getCoins(){
        $coins = Coin::all();
        return response()->json([
            'data' => $coins,
        ]);
    }



    // test live price update from binance
    public function livePrice()
    {
        $coin = Coin::all();
        foreach ($coin as $c){
            $name = $c->name;
            $conName = $name.'USDT';
            $client =  new Client();
            $response = $client->request('GET', 'https://api.binance.com/api/v3/ticker/price?symbol='.$conName);
            $arr = json_decode($response->getBody()->getContents(), true);
            $c = Coin::query()->where('name', $name);
            $rial = $arr['price'] * 30500.000;
            $c->update(['usd_price' => $arr['price']]);
            $c->update(['rial_price' => $rial]);
        }
    }
}
