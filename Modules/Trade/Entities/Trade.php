<?php

namespace Modules\Trade\Entities;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;
use Modules\Coin\Entities\Coin;
use Modules\Finance\Entities\Transaction;
use Modules\Finance\Entities\UserWallet;

class Trade extends Model
{
    use HasFactory, SoftDeletes;

    protected $appends = ['market_label', 'status_label', 'type_label'];
    protected $table = 'trades';
    protected $fillable = [
        'coin_id',
        'user_id',
        'amount',
        'done_amount',
        'market_type',
        'status',
        'type',
        'wage',
        'ip',
        'expire_date',
        'price',
        'total_price',
    ];

    const RIAL_MARKET_TYPE = 0;
    const USD_MARKET_TYPE = 1;

    const BUY_TYPE = 1;
    const SELL_TYPE = 2;

    const PENDING_STATUS = 1;
    const SUCCESS_STATUS = 2;
    const CANCEL_STATUS = 3;
    const FAILED_STATUS = 4;

    protected static function newFactory()
    {
        return \Modules\Trade\Database\factories\TradeFactory::new();
    }

    public function getMarketLabelAttribute(){
        switch ($this->market_type){
            case self::RIAL_MARKET_TYPE:
                return 'Rial';
                break;
            case self::USD_MARKET_TYPE:
                return 'USD';
                break;
        }
    }

    public function getStatusLabelAttribute(){
        switch ($this->status){
            case self::PENDING_STATUS:
                return 'Pending';
                break;
            case self::SUCCESS_STATUS:
                return 'Active';
                break;
            case self::CANCEL_STATUS:
                return 'Cancel';
                break;
            case self::FAILED_STATUS:
                return 'Failed';
                break;
        }
    }

    public function getTypeLabelAttribute(){
        switch ($this->type){
            case self::BUY_TYPE:
                return 'Buy';
                break;
            case self::SELL_TYPE:
                return 'Sell';
                break;
        }
    }

    public function listTrades($request){
        $status = $request->get('filter_status');
        if($status){
           $trades = self::query()->where('status', $status)->latest()->paginate(15);
           return $trades;
        }  else {
            return $trades = self::query()->latest()->paginate(15);
        }
    }

    public function tradeType($request){
        $tradeType = $request->get('filter_type');
        if($tradeType){
           $trades = self::query()->where('type', $tradeType)->latest()->paginate(15);
           return $trades;
        }  else {
            return $trades = self::query()->latest()->paginate(15);
        }
    }

    public static function checkWalletBalance($user_id, $price){
        $wallet = UserWallet::query()
            ->where('owner_id', $user_id)
            ->sum('amount');
        if($wallet >= $price){
            return true;
        } else {
            return false;
        }
    }


    public function coin(){
        return $this->belongsTo(Coin::class, 'coin_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions(){
        return $this->hasMany(Transaction::class, 'trade_id');
    }

}
