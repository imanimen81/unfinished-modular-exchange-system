<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Trade\Http\Controllers\V2\TradeController;
use Modules\Trade\Http\Controllers\V2\TradeDataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v2/trades'], function ($router){

    $router->get('/get-trades', [TradeDataController::class, 'userTrades']);

    $router->get('/list-trades', [TradeDataController::class, 'userTradesList']);

    // trade buy and sell in rial market
    $router->post('/buy-trade-rial', [TradeController::class, 'buyTradeRial']);
    $router->post('/sell-trade-rial', [TradeController::class, 'sellTradeRial']);

    // trade buy and sell in usd market
    $router->post('/buy-trade-usd', [TradeController::class, 'buyTradeUsd']);
    $router->post('/sell-trade-usd', [TradeController::class, 'sellTradeUsd']);

    // trade data
    $router->get('/home', [TradeDataController::class, 'tradeData']);
    $router->get('/get-trades-by-type', [TradeDataController::class, 'getTradesByType']);

    // trade history
    $router->get('/trade-history', [TradeDataController::class, 'tradeHistory']);

    // last three months data
    $router->get('/last-three-months-data', [TradeDataController::class, 'lastThreeMonthTradesRial']);

    // user properties
    $router->get('/user-properties', [TradeDataController::class, 'userProperties']);

    // get data between dates
    $router->post('/get-trades-by-date', [TradeDataController::class, 'getTradesDateData']);

});
