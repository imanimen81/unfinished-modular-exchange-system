<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignId('coin_id')->references('id')->on('coins');
            $table->double('amount')->nullable();
            $table->double('done_amount')->nullable();
            $table->double('price')->nullable();
            $table->double('total_price')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->tinyInteger('market_type')->nullable();
            $table->double('wage')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('ip')->nullable();
            $table->string('expire_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
