<?php

namespace Modules\Trade\Http\Controllers\V2;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as Request;
use Illuminate\Routing\Controller;
use Modules\Coin\Entities\Coin;
use Modules\Trade\Entities\Trade;
use Modules\Trade\Http\Requests\GetTradesDateRequest as GetTradesDateRequest;
use Modules\Trade\Http\Traits\TradeData;

class TradeDataController extends Controller
{
    use TradeData;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['tradeData']]);
    }


    // user trades
    public function userTrades(Request $request)
    {
        try {
            $user = auth()->user();
            $trades = Trade::query()->where('user_id', $user->id)->paginate(15);
            return response()->json([
                'data'  => [
                    'trades' => $trades,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }


    // get data from trades
    public function tradeData(): JsonResponse
    {
        try {
            $coins = Coin::all();
            foreach ($coins as $coin) {
                list($count, $buyPriceRial, $sellPriceRial, $countUsd, $buyPriceUsd, $sellPriceUsd,
                    $max24hours, $min24hours, $turnOverCountRial,$turnOverCountUsd, $max24hoursUsd, $min24hoursUsd, $priceChange, $priceChangeUsd) = $this->getData($coin);
                $rialData[] = [
                    'coin' => $coin->name,
                    'key' => $coin->key,
                    'icon' => $coin->icon,
                    'icon_url' => $coin->icon_url,
                    'buyPriceRial' => $buyPriceRial,
                    'sellPriceRial' => $sellPriceRial,
                    'max24Hours' => $max24hours,
                    'min24Hours' => $min24hours,
                    'volume' => $turnOverCountRial,
                    'priceChange' => '%'.$priceChange
                ];
                $usdData[] = [
                    'coin' => $coin->name,
                    'key' => $coin->key,
                    'icon' => $coin->icon,
                    'icon_url' => $coin->icon_url,
                    'buyPriceUsd' => $buyPriceUsd,
                    'sellPriceUsd' => $sellPriceUsd,
                    'max24Hours' => $max24hoursUsd,
                    'min24Hours' => $min24hoursUsd,
                    'volume' => $turnOverCountUsd,
                    'priceChange' => '%'.$priceChangeUsd
                ];
            }
            return response()->json([
                'data' => [
                    'rialMarket' => $rialData,
                    'usdMarket' => $usdData,
                    'tradeCountRial' => $count,
                    'tradeCountUsd' => $countUsd,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }


    // trade history
    public function tradeHistory(Request $request)
    {
        try {
            $user = auth()->user();
            $data = $user->trades()->paginate(15);
            return response()->json([
                'data' => [
                    'trades' => $data,
                    'trades_count' => $data->count()
                ]
            ]);

        } catch (\Exception $e) {
            return response()->json([
                    'errors' => [
                        'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    public function lastThreeMonthTradesRial()
    {
        try {
            $user = auth()->user();
            $rialAmount = $this->last3MonthAmountRial();
            $usdAmount = $this->last3MonthAmountUsd();

            return response()->json([
                'data' => [
                    'rial' => $rialAmount,
                    'usd' => $usdAmount,
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function userTradesList(){
        try {
            $user = auth()->user();
            $trades = $this->filterUserTrades();
            return response()->json([
                'data' => [
                    'trades' => $trades,
                    'trades_count' => $trades->count()
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function getTradesByType(Request $request){
        try {
            $trades = $this->tradeType($request);
            return response()->json([
                'data' => [
                    'trades' => $trades,
                    'trades_count' => $trades->count()
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }
    // user properties
    public function userProperties(){
        try {
            $rialProperties = $this->rialProperties();
            $usdProperties = $this->usdProperties();
            $coins = $this->coinProperties();
            return response()->json([
                'data' => [
                    'rial_property' => $rialProperties,
                    'usd_property' => $usdProperties,
                    'coins' => $coins,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public  function getTradesDateData(GetTradesDateRequest $request){
        try {
            $user_id = auth()->user()->id;
            $startDate = $request->get('start_date');
            $endDate = $request->get('end_date');
            $marketType = $request->get('market_type');
            $data = $this->getTradesDataBetweenDates($user_id, $startDate, $endDate, $marketType);

            return response()->json([
                'data' => [
                    'trades' => $data,
                    'tradeCount' => $data->count()
                ]
            ], 200);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }


    /**
     * @param $coin
     * @return array
     */
    protected function getData($coin): array
    {
        $count = $this->getRialTradeCount($coin);
        $buyPriceRial = $this->getLastBuyTradePriceRial($coin->id);
        $sellPriceRial = $this->getLastSellTradePriceRial($coin->id);
        $countUsd = $this->getUsdTradeCount();
        $buyPriceUsd = $this->getLastBuyTradePriceUsd($coin->id);
        $sellPriceUsd = $this->getLastSellTradePriceUsd($coin->id);
        $max24hours = $this->maxPriceLast24HoursRial($coin->id);
        $min24hours = $this->minPriceLast24HoursRial($coin->id);
        $turnOverCountRial = $this->turnOverCountRial($coin->id);
        $turnOverCountUsd = $this->turnOverCountUsd($coin->id);
        $min24hoursUsd = $this->minPriceLast24HoursUsd($coin->id);
        $max24hoursUsd = $this->maxPriceLast24HoursUsd($coin->id);
        $priceChange = $this->priceChangeRial($coin->id);
        $priceChangeUsd = $this->priceChangeUsd($coin->id);
        return array($count, $buyPriceRial, $sellPriceRial, $countUsd, $buyPriceUsd, $sellPriceUsd, $max24hours, $min24hours, $turnOverCountRial, $turnOverCountUsd, $min24hoursUsd,$max24hoursUsd, $priceChange, $priceChangeUsd);
    }
}
