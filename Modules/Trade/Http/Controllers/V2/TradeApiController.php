<?php

namespace Modules\Trade\Http\Controllers\V2;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Trade\Http\Services\crypto;

class TradeApiController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth:api');
//    }

    // test api call
    public function apiTest()
    {
        $api = crypto::test();
        return response()->json($api);
    }
}
