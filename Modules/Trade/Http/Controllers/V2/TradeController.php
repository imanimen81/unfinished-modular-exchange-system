<?php

namespace Modules\Trade\Http\Controllers\V2;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Coin\Entities\Coin;
use Modules\Finance\Entities\Transaction;
use Modules\Finance\Entities\UserWallet;
use Modules\Trade\Entities\Trade;
use Modules\Wallet\Entities\Wallet;

class TradeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // try trade
    public function buyTradeRial(Request $request){
        try {
            $user = auth()->user();
            $coin = $this->getCoin($request);
            $amount = $request->get('amount');
            $coinPrice = $coin->rial_price;
            $totalPrice = $amount * $coinPrice;
            $checkWallet = Trade::checkWalletBalance($user->id, $totalPrice);
            if ($checkWallet == true){
                $trade = Trade::query()
                    ->create([
                        'user_id' => $user->id,
                        'coin_id' => $coin->id,
                        'amount' => $amount,
                        'type' => Trade::BUY_TYPE,
                        'done_amount' => $amount,
                        'market_type' => Trade::RIAL_MARKET_TYPE,
                        'status' => Trade::PENDING_STATUS,
                        'wage' => $user->level->wage_buyer,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'total_price' => $totalPrice
                    ]);
                $transaction = Transaction::query()->create([
                    'user_wallet_id' => $this->getWallet($request),
                    'amount' => $amount,
                    'status' => Transaction::PENDING_STATUS,
                    'tag' => Transaction::BUY_TRADE,
                    'wage' => $user->level->wage_buyer,
                    'accept_date' => Carbon::now(),
                    'expire_date' => Carbon::now()->addDays(1),
                    'item_type' => Trade::class,
                    'item_id' => $trade->id,
                    'creator_type' => User::class,
                    'payment_method' => Transaction::WALLET_PAYMENT,
                    'creator_id' => $user->id
                ]);
            } if ($checkWallet == false) {
                $trade = Trade::query()
                    ->create([
                        'user_id' => $user->id,
                        'coin_id' => $coin->id,
                        'amount' => $amount,
                        'type' => Trade::BUY_TYPE,
                        'done_amount' => $amount,
                        'market_type' => Trade::RIAL_MARKET_TYPE,
                        'status' => Trade::PENDING_STATUS,
                        'wage' => $user->level->wage_buyer,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'total_price' => $totalPrice
                    ]);
                $transaction = Transaction::query()->create([
                    'user_wallet_id' => $this->getWallet($request),
                    'amount' => $amount,
                    'status' => Transaction::PENDING_STATUS,
                    'tag' => Transaction::BUY_TRADE,
                    'wage' => $user->level->wage_buyer,
                    'accept_date' => Carbon::now(),
                    'expire_date' => Carbon::now()->addDays(1),
                    'item_type' => Trade::class,
                    'item_id' => $trade->id,
                    'creator_type' => User::class,
                    'payment_method' => Transaction::BANK_PAYMENT,
                    'creator_id' => $user->id
                ]);
            }

            return response()->json([
                'message' => 'Your Trade has been added successfully',
                'status' => 'Pending',
                'data' => [
                    'trade' => $trade,
                    'transaction' => $transaction
                ]
            ], 201);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }


    // sell trade
    public function sellTradeRial(Request $request){
        try {
            $user = auth()->user();
            $coin = $this->getCoin($request);
            $amount = $request->get('amount');
            $coinPrice = $coin->rial_price;
            $totalPrice = $amount * $coinPrice;
            $trade = Trade::query()
                ->create([
                    'user_id' => $user->id,
                    'coin_id' => $coin->id,
                    'amount' => $amount,
                    'type' => Trade::SELL_TYPE,
                    'done_amount' => $amount,
                    'market_type' => Trade::RIAL_MARKET_TYPE,
                    'status' => Trade::PENDING_STATUS,
                    'wage' => $user->level->wage_seller,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'total_price' => $totalPrice
                ]);
            $transaction = Transaction::query()->create([
                'user_id' => $user->id,
                'coin_id' => $coin->id,
                'wallet_id' => $this->getWallet($request),
                'amount' => $amount,
                'status' => Transaction::PENDING_STATUS,
                'tag' => Transaction::SELL_TRADE,
                'wage' => $user->level->wage_seller,
                'accept_date' => Carbon::now(),
                'expire_date' => Carbon::now()->addDays(1),
                'item_type' => Trade::class,
                'item_id' => $trade->id,
                'creator_type' => User::class,
                'creator_id' => $user->id
            ]);
            return response()->json([
                'message' => 'Your Trade has been added successfully',
                'status' => 'Pending',
                'data' => [
                    'trade' => $trade,
                    'transaction' => $transaction
                ]
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    // usd buy trade
    public function buyTradeUsd(Request $request){
        try {
            $user = auth()->user();
            $coin = $this->getCoin($request);
            $amount = $request->get('amount');
            $coinPrice = $coin->usd_price;
            $totalPrice = $amount * $coinPrice;
            $trade = Trade::query()
                ->create([
                    'user_id' => $user->id,
                    'coin_id' => $coin->id,
                    'amount' => $amount,
                    'type' => Trade::BUY_TYPE,
                    'done_amount' => $amount,
                    'market_type' => Trade::USD_MARKET_TYPE,
                    'status' => Trade::PENDING_STATUS,
                    'wage' => $user->level->wage_buyer,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'total_price' => $totalPrice
                ]);
            $transaction = Transaction::query()->create([
                'user_id' => $user->id,
                'coin_id' => $coin->id,
                'wallet_id' => $this->getWallet($request),
                'amount' => $amount,
                'status' => Transaction::PENDING_STATUS,
                'tag' => Transaction::BUY_TRADE,
                'wage' => $user->level->wage_buyer,
                'accept_date' => Carbon::now(),
                'expire_date' => Carbon::now()->addDays(1),
                'item_type' => Trade::class,
                'item_id' => $trade->id,
                'creator_type' => User::class,
                'creator_id' => $user->id
            ]);
            return response()->json([
                'message' => 'Your Trade has been added successfully',
                'status' => 'Pending',
                'data' => [
                    'trade' => $trade,
                    'transaction' => $transaction
                ]
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    // sell trade usd
    public function sellTradeUsd(Request $request){
        try {
            $user = auth()->user();
            $coin = $this->getCoin($request);
            $amount = $request->get('amount');
            $coinPrice = $coin->usd_price;
            $totalPrice = $amount * $coinPrice;
            $trade = Trade::query()
                ->create([
                    'user_id' => $user->id,
                    'coin_id' => $coin->id,
                    'amount' => $amount,
                    'type' => Trade::SELL_TYPE,
                    'done_amount' => $amount,
                    'market_type' => Trade::USD_MARKET_TYPE,
                    'status' => Trade::PENDING_STATUS,
                    'wage' => $user->level->wage_buyer,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'total_price' => $totalPrice
                ]);
            $transaction = Transaction::query()->create([
                'coin_id' => $coin->id,
                'wallet_id' => $this->getWallet($request),
                'amount' => $amount,
                'status' => Transaction::PENDING_STATUS,
                'tag' => Transaction::SELL_TRADE,
                'wage' => $user->level->wage_buyer,
                'accept_date' => Carbon::now(),
                'expire_date' => Carbon::now()->addDays(1),
                'item_type' => Trade::class,
                'item_id' => $trade->id,
                'creator_type' => User::class,
                'creator_id' => $user->id
            ]);
            return response()->json([
                'message' => 'Your Trade has been added successfully',
                'status' => 'Pending',
                'data' => [
                    'trade' => $trade,
                    'transaction' => $transaction
                ]
            ], 201);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ]);
        }
    }

    protected function getCoin(Request $request)
    {
        return Coin::query()
            ->where('id', $request->get('coin_id'))
            ->firstOrFail();
    }

    protected function getWallet(Request $request){
        return UserWallet::query()
            ->where('owner_id', auth()->user()->id)
            ->where('id', $request->get('wallet_id'))
            ->firstOrFail()->id;
    }

}
