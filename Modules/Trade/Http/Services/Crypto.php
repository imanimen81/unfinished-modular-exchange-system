<?php

namespace Modules\Trade\Http\Services;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class crypto
{
    // test api call
    public static function tsest()
    {
        $url = 'https://kamexapi.ir/api/v1/bnb/payment';
        $post = [
            'receiver' => '1234',
            'amount' => '1234',
            'code' => '1234',
            'tag' => '1234',
        ];
        $token = env('API_WALLET_KEY');
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, [
            // content-type header
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],

            'form_params' => [
                'receiver' => $post['receiver'],
                'amount' => $post['amount'],
                'code' => $post['code'],
                'tag' => $post['tag'],
            ],
        ]);
        $body = $response->getBody()->getContents();
        dd($body);
        $body = json_decode($body, true);
        return $body;
    }

    public static function test()
    {
        $url = "https://kamexapi.ir/api/v1/" . 'eth' . "/payment";
        echo $url;
        $data = [
            'receiver' => '1231234',
            'tag' =>  '1231414',
            'amount' => '0.1',
            'code' => 'kx-123123',
        ];
        $httpClient = new Client();

        $response = $httpClient->post(
            $url,
            [
                RequestOptions::BODY => json_encode($data),
                RequestOptions::HEADERS => [
                    'Content-Type' => 'application/json',
                    'Authorization' => "Bearer " . env('API_WALLET_KEY'),
                ],
            ]
        );

        $res = $response->getBody()->getContents();
        $res = json_decode($res);
    }
}
