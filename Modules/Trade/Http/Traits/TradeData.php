<?php

namespace Modules\Trade\Http\Traits;

use Carbon\Carbon;
use Modules\Coin\Entities\Coin;
use Modules\Trade\Entities\Trade;
use Morilog\Jalali\Jalalian;

trait TradeData
{
    public function getRialTradeCount(){
        return Trade::query()
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->where('status', Trade::SUCCESS_STATUS)
            ->count();
    }

    public function getUsdTradeCount(){
        return Trade::query()
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->where('status', Trade::SUCCESS_STATUS)
            ->count();
    }

    public  function getLastBuyTradePriceRial($coin_id){
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->where('type', Trade::BUY_TYPE)
            ->where('status', Trade::SUCCESS_STATUS)
            ->latest()
            ->value('total_price');
    }

    public  function getLastSellTradePriceRial($coin_id)
    {
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->where('type', Trade::SELL_TYPE)
            ->where('status', Trade::SUCCESS_STATUS)
            ->latest()
            ->value('total_price');
    }


    public static function getLastBuyTradePriceUsd($coin_id){
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->where('type', Trade::BUY_TYPE)
            ->where('status', Trade::SUCCESS_STATUS)
            ->latest()
            ->value('total_price');
    }

    public static function getLastSellTradePriceUsd($coin_id){
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->where('type', Trade::SELL_TYPE)
            ->where('status', Trade::SUCCESS_STATUS)
            ->latest()
            ->value('total_price');
    }

    public static function turnOverCountRial($coin_id){
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->sum('amount');
    }

    public static function turnOverCountUsd($coin_id){
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->sum('amount');
    }

    public static function maxPriceLast24HoursRial($coin_id){
        $yesterday = Carbon::now()->subDay()->toDateTimeString();
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->where('created_at', '>=', $yesterday)
            ->max('total_price');
    }
    public static function minPriceLast24HoursRial($coin_id){
        $yesterday = Carbon::now()->subDay()->toDateTimeString();
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->where('created_at', '>=', $yesterday)
            ->min('total_price');
    }

    public static function maxPriceLast24HoursUsd($coin_id){
        $yesterday = Carbon::now()->subDay()->toDateTimeString();
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->where('created_at', '>=', $yesterday)
            ->max('total_price');
    }

    public static function minPriceLast24HoursUsd($coin_id){
        $yesterday = Carbon::now()->subDay()->toDateTimeString();
        return Trade::query()
            ->where('coin_id', $coin_id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->where('created_at', '>=', $yesterday)
            ->min('total_price');
    }

    public static function priceChangeRial($coin_id){
        $coins = Coin::query()->where('id', $coin_id)->first();

        if (count($coins->trades) > 0 ) {
            $today = Trade::query()
                ->where('coin_id', $coin_id)
                ->where('status', Trade::SUCCESS_STATUS)
                ->where('market_type', Trade::RIAL_MARKET_TYPE)
                // today
                ->where('created_at', '>=', Carbon::now()->startOfDay()->toDateTimeString())
                ->latest()
                ->value('total_price');

            $yesterday = Trade::query()
                ->where('coin_id', $coin_id)
                ->where('status', Trade::SUCCESS_STATUS)
                ->where('market_type', Trade::RIAL_MARKET_TYPE)
                ->where('created_at', '<=', Carbon::now()->addDays(-1)->toDateTimeString())
                ->latest()
                ->value('total_price');
            if($today > 0 && $yesterday > 0){
                $percent = round(($today - $yesterday) / $yesterday * 100) / 100;
                return $percent;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public static function priceChangeUsd($coin_id){
        $coins = Coin::query()->where('id', $coin_id)->first();

        if (count($coins->trades) > 0 ){
            $today =  Trade::query()
                ->where('coin_id', $coin_id)
                ->where('status', Trade::SUCCESS_STATUS)
                ->where('market_type', Trade::USD_MARKET_TYPE)
                ->latest()
                ->value('total_price');

            $yesterday = Trade::query()
                ->where('coin_id', $coin_id)
                ->where('status', Trade::SUCCESS_STATUS)
                ->where('market_type', Trade::USD_MARKET_TYPE)
                ->where('created_at', '<=', Carbon::now()->addDays(-1)->toDateTimeString())
                ->latest()
                ->value('total_price');

            if($today > 0 && $yesterday > 0){
                $percent = round(($today - $yesterday) / $yesterday * 100) / 100;
                return $percent;
            } else {
                return 0;
            }
        }
        else{
            return 0;
        }
    }

    public static function last3MonthAmountRial()
    {
        $lastMonth = Carbon::now()->subMonth(3)->toDateTimeString();
        return Trade::query()
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->where('type', Trade::BUY_TYPE)
            ->where('created_at', '>=', $lastMonth)
            ->sum('total_price');
    }

    public static function last3MonthAmountUsd(){
        $lastMonth = Carbon::now()->subMonth(3)->toDateTimeString();
        return Trade::query()
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->where('type', Trade::BUY_TYPE)
            ->where('created_at', '>=', $lastMonth)
            ->sum('total_price');
    }

    public function filterUserTrades(){
        $trades = Trade::query();
        $trades->when(request('filter_status') == Trade::SUCCESS_STATUS, function ($query) {
            $query->where('user_id', auth()->user()->id)->where('status', Trade::SUCCESS_STATUS);
        });
        $trades->when(request('filter_status') == Trade::CANCEL_STATUS, function ($query) {
            $query->where('user_id', auth()->user()->id)->where('status', Trade::CANCEL_STATUS);
        });

        $trades->when(request('filter_status') == Trade::PENDING_STATUS, function ($query) {
            $query->where('user_id', auth()->user()->id)->where('status', Trade::PENDING_STATUS);
        });
        $trades->when(request('filter_status') == Trade::FAILED_STATUS, function ($query) {
            $query->where('user_id', auth()->user()->id)->where('status', Trade::FAILED_STATUS);
        });
        $data = $trades->latest()->paginate(15);
        return $data;
    }

    public function tradeType($request){
        $tradeType = $request->get('filter_type');
        if($tradeType){
            $trades = Trade::query()
                ->where('user_id', auth()->user()->id)
                ->where('type', $tradeType)
                ->latest()->paginate(15);
            return $trades;
        }  else {
            return $trades = Trade::query()
                ->where('user_id', auth()->user()->id)
                ->latest()->paginate(15);
        }
    }


    // user properties trades
    public function rialProperties(){
        $trades = Trade::query()
            ->where('user_id', auth()->user()->id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::RIAL_MARKET_TYPE)
            ->sum('total_price');
        return $trades;
    }

    public function usdProperties(){
        $trades = Trade::query()
            ->where('user_id', auth()->user()->id)
            ->where('status', Trade::SUCCESS_STATUS)
            ->where('market_type', Trade::USD_MARKET_TYPE)
            ->sum('total_price');
        return $trades;
    }

    public function coinProperties(){
        $user = auth()->user();
        $trades = $user->trades()->where('status', Trade::SUCCESS_STATUS)->get();
        foreach ($trades as $trade){
           $coins [] = [
                'coin_id' => $trade->coin_id,
                'name' => $trade->coin->name,
                'icon' => $trade->coin->icon_url,
                'amount' => $trade->done_amount,
           ];
        }
        return $coins;

    }
    public function getTradesDataBetweenDates($user_id, $startDate, $endDate, $marketType){
        return Trade::query()
            ->where('user_id', $user_id)
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where('market_type', $marketType)
            ->get();
    }

}
