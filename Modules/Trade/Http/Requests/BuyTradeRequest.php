<?php

namespace Modules\Trade\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuyTradeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required',
            'coin_id' => 'required|numeric|exists:coins,id',
            'wallet_id' => 'required|numeric|exists:user_wallets,id',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
