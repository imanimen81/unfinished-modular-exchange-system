<?php

namespace Modules\Wallet\Http\Controllers\V2;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Coin\Entities\Coin;
use Modules\Wallet\Entities\Wallet;
use Modules\Wallet\Http\Requests\AddWalletRequest as AddWalletRequest;

class WalletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // add wallet
    public function addWallet(AddWalletRequest $request){
        try {
            $user = auth()->user();
            $coin = $this->getCoin($request);
            $data = $request->all();
            $data['user_id'] = $user->id;
            $data['coin_id'] = $coin->id;
            $wallet = Wallet::query()->create($data);
            return response()->json([
                'message' => 'Wallet Added successfully',
                'wallet' => $wallet
            ], 201);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    // get wallet
    public function getWalletByCoin(Request $request){
        try {
            $user = auth()->user();
            $coin = $this->getCoin($request);
            $wallet = $user->wallets;
            return response()->json([
                'data' => $wallet,
            ], 200);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    public function getWallets(Request $request){
        try {
            $user = auth()->user();
            $wallets = $user->wallets;
            return response()->json([
                'data' => $wallets,
            ], 200);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }


    // delete wallet
    public function deleteWallet(Request $request){
        try {
            $user = auth()->user();
            $wallet = $this->getWallet($request);
            $wallet->delete();
            return response()->json([
                'message' => 'Wallet deleted successfully',
                'data' => [
                    'wallet' => $wallet
                ]
            ], 200);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    protected function getCoin(Request $request){
        $coin = Coin::query()
            ->where('id', $request->get('coin_id'))
            ->orWhere('name', $request->get('name'))
            ->firstOrFail();
        return $coin;
    }

    protected function getWallet(Request $request){
        return Wallet::query()
            ->where('id', $request->get('id'))
            ->orWhere('address', $request->get('address'))
            ->firstOrFail();
    }
}
