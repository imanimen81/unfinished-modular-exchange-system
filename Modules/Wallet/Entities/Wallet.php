<?php

namespace Modules\Wallet\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Coin\Entities\Coin;
use Modules\Finance\Entities\Transaction;

class Wallet extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'coin_id',
        'address',
        'balance',
        'tag',
        'status',
    ];
    protected $table = 'wallets';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected static function newFactory()
    {
        return \Modules\Wallet\Database\factories\WalletFactory::new();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function coin(){
        return $this->belongsTo(Coin::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
}
