<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Wallet\Http\Controllers\V2\WalletController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => '/v2/wallet'], function ($router) {
    $router->post('/add-wallet', [WalletController::class, 'addWallet']);
    $router->get('/get-wallets', [WalletController::class, 'getWallets']);
    $router->post('/get-wallet-by-coin', [WalletController::class, 'getWalletByCoin']);
    $router->post('/delete-wallet', [WalletController::class, 'deleteWallet']);
});
