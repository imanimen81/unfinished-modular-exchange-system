<?php

namespace Modules\Auth\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CheckYear implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $carbon = Carbon::parse($value);
        if ($carbon->subYears(18)->format('Y-m-d') < date('Y-m-d')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'age must be greater than 18';
    }
}
