<?php

namespace Modules\Auth\Http\Controllers\V2;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\UserFile;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Http\Requests\UploadFilesRequest as UploadFilesRequest;

class UploadUserFilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // upload user files
    public function uploadFiles(UploadFilesRequest $request){
        try {
            $check = UserFile::query()->where('user_id', auth()->user()->id)->first();
            if ($check){
                return response()->json(['errors' => [
                    'message' => 'You have uploaded your files before!'
                ]], 422);
            }
                $year = date("Y");
                $month = date("m");

                // national_card_image
                $fileNameWithExtNational = $request->file('national_card_image')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExtNational,  PATHINFO_FILENAME);
                $extension = $request->file('national_card_image')->getClientOriginalExtension();
                $fileNameToStoreNational = $fileName.'_'.time().'.'.$extension;
                $path_national = $request->file('national_card_image')->storeAs('public/documents'.'/'.$year.'/'.$month, $fileNameToStoreNational);

                // selfie_image
                $fileNameWithExtSelfie = $request->file('selfie_image')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExtSelfie,  PATHINFO_BASENAME);
                $extension = $request->file('selfie_image')->getClientOriginalExtension();
                $fileNameToStoreSelfie = $fileName.'_'.time().'.'.$extension;
                $path = $request->file('selfie_image')->storeAs('public/documents'.'/'.$year.'/'.$month, $fileNameToStoreSelfie);

                $user_id = auth()->user()->id;
                $update = UserFile::query()->create([
                    'user_id' => $user_id,
                    'national_card_image' => 'storage/documents/'.$year.'/'. $month . '/' . $fileNameToStoreNational,
                    'selfie_image' => 'storage/documents/'.$year.'/'. $month . '/' . $fileNameToStoreSelfie,
                    'expire_date' => Carbon::now()->addDays(5),
                    'is_confirmed' => false
                ]);
                return response()->json([
                    'data' => $update
                ], 201);
        } catch (\Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ]);
        }
    }
}
