<?php

namespace Modules\Auth\Http\Controllers\V2;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\MobileOtp;
use Modules\Auth\Entities\Profile;
use Modules\Auth\Http\Requests\UpdateProfileRequest as UpdateProfileRequest;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'password_change']);
    }

    // test update user
    public function updateUserLevel2(UpdateProfileRequest $request){
        try {
            $user = auth()->user();
            $checkProfile = Profile::query()->where('user_id', $user->id)->first();
           if (!$checkProfile) {
                $data = [
                    'user_id' => $user->id,
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'national_code' => $request->get('national_code'),
                    'mobile' => $request->get('mobile'),
                    'birth_date' => $request->get('birth_date'),
                    'address' => $request->get('address'),
                    'post_code' => $request->get('post_code'),
                    'status' => Profile::STATUS_PENDING,
                ];
                $type = MobileOtp::TYPE_UPDATE_PROFILE;
                $sendOtp = MobileOtp::sentOtp($data['mobile'], true, $type);
                $profile = Profile::query()->create($data);
                return response()->json([
                    'message' => 'Check your mobile for OTP',
                    'code' => $sendOtp,
                ]);
            } else {
                return response()->json([
                    'errors' => [
                        'message' => 'You have already submitted your profile',
                    ],
                ], 422);
            }
        }  catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function verifyUserLevel2(Request $request){
        try {
            $user = auth()->user();
            $code = $request->get('code');
            $profile = Profile::query()->where('user_id', $user->id)->first();
            // dd($profile);
            $checkCode = MobileOtp::checkCode($profile->mobile, $code);
            if ($checkCode){
                $profile->update([
                    'status' => Profile::STATUS_WAITING_FOR_VERIFICATION,
                ]);
                $profile->save();
                $user->update(['status' => User::STATUS_PENDING]);
                return response()->json([
                    'message' => 'Profile updated successfully. upload your files please',
                    'data' => $profile
                ]);
            }
            $checkProfile = Profile::query()->where('user_id', $user->id)->first();
            if ($checkProfile){
                return response()->json([
                    'errors' => [
                        'message' => 'You have already submitted your profile',
                    ],
                ], 422);
            }
            if (!$checkCode){
                $profile->delete();
                return response()->json([
                    'errors' => [
                        'message' => 'Invalid code'
                    ]
                ], 422);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }



    // get user profile info by user level
    public function getProfile()
    {
        try {
            $user = auth()->user();
            $profile = Profile::getUserProfileAttribute();
            return response()->json([
                'data' => $profile
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    // update user profile
    public function updateLevelThree(Request $request){
        try {
            $profile = Profile::query()->where('user_id', auth()->user()->id)->first();
            $profile->update([
                'city' => $request->get('city'),
                'address' => $request->get('address'),
                'post_code' => $request->get('post_code'),
                'phone' => $request->get('phone'),
            ]);
            $profile->save();
            return response()->json([
                'data' => $profile
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }


    public function getUserProfileLevel2()
    {
        try {
            $profile = Profile::query()
                ->where('user_id', auth()->user()->id)
                ->firstOrFail();
            return response()->json([
                'data' => $profile,
            ], 200);
        } catch (\Exception $e){
            return response()->json([
               'errors' => [
                   'message' => $e->getMessage()
               ]
            ], 500);
        }
    }

}
