<?php

namespace Modules\Auth\Http\Controllers\V2;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\EmailOtp;
use Modules\Auth\Entities\Level;
use Modules\Auth\Entities\MobileOtp;
use Modules\Auth\Http\Requests\RegisterRequest as RegisterRequest;
use Modules\Auth\Jobs\SendMobileOtpJob;
use Modules\Auth\Jobs\SendOtpEmailJob;

class OtpRegisterController extends Controller
{

    // send otp code
    public function otpRegister(RegisterRequest $request)
    {
       $email = $request->get('email');
       $mobile = $request->get('mobile');

        if ($request->has('email') and $request->has('mobile')) {
              return response()->json([
                'errors' => [
                     'message' => 'You can not use both email and mobile'
                ]
              ], 422);
        }

        if ($request->has('mobile')){
            $type = MobileOtp::TYPE_REGISTER;
            if ($mobile){
                $sendMobileOtp = MobileOtp::sentOtp($mobile, true, $type);
            if ($sendMobileOtp){
//                dispatch(new SendMobileOtpJob($mobile, $sendMobileOtp));
                return response()->json([
                    'data' => [
                        'message' => 'Otp sent successfully',
                        'code' => $sendMobileOtp // todo: change to api call response
                    ]
                ], 200);
            }
            if ($sendMobileOtp == null){
                return response()->json([
                    'errors' => [
                        'message' => 'Otp not sent'
                    ]
                ], 200);
            }
            }
        }

        if ($request->has('email')){
            if ($email){
            $type = EmailOtp::TYPE_REGISTER;
            $sendEmailOtp = EmailOtp::sentOtp($email, true, $type);
//            SendOtpEmailJob::dispatch($email, $sendEmailOtp);
            if ($sendEmailOtp){
                dispatch(new SendOtpEmailJob($email, $sendEmailOtp));
                return response()->json([
                    'data' => [
                        'message' => 'Otp sent successfully',
                        'code' => $sendEmailOtp // todo: change to api call response
                    ]
                ], 200);
            }
            if ($sendEmailOtp == null){
                return response()->json([
                    'errors' => [
                        'message' => 'Otp not sent'
                    ]
                ], 200);
            }
            }
        }

        else {
            return response()->json([
                'errors' => [
                    'message' =>'You must provide email or mobile!'
                ]
            ], 422);
        }
    }

    // verify and register

    public function otpRegisterVerify(RegisterRequest $request)
    {
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        $code_email = $request->get('code_email');
        $code_mobile = $request->get('code_mobile');
        $password = $request->get('password');

        if ($request->has('email') and $request->has('mobile')) {
            return response()->json([
              'errors' => [
                   'message' => 'You can not use both email and mobile'
              ]
            ], 422);
        }

        if ($request->has('email')){
            $type = EmailOtp::TYPE_REGISTER;
//            $emailOtp = EmailOtp::checkCode($email, $code_mobile);
            $emailOtp = EmailOtp::checkCode($email, $request->get('code'));
//            query()->where('email', $email)->where('code', $code_email)->where('type', $type)->first();

            if ($emailOtp){
                $createUser = User::query()->create([
                    'email' => $email,
                    'mobile' => $mobile,
                    'password' => Hash::make($password),
                    'level_id' => Level::query()->where('level', 1)->first()->id,
                    'status' => User::STATUS_NEED_TO_VERIFY

                ]);
            }
                if (!$token = auth()->attempt(['email' => $email, 'password' => $password])) {
                    return response()->json([
                        'errors' => [
                            'message' => 'Unauthorized'
                        ]
                    ], 401);
                }
                if ($emailOtp == null){
                    return response()->json([
                        'errors' => [
                            'message' => 'Invalid Code!'
                        ]
                    ], 401);
                }
                return $this->createNewToken($token)->original;
            }

        if ($request->has('mobile')){
            $type = MobileOtp::TYPE_REGISTER;
            $mobileOtp = MobileOtp::checkCode($mobile, $request->get('code'));
            if ($mobileOtp){
                $createUser = User::query()->create([
                    'email' => $email,
                    'mobile' => $mobile,
                    'password' => Hash::make($password),
                    'level_id' => Level::query()->where('level', 1)->first()->id,
                    'status' => User::STATUS_NEED_TO_VERIFY
                ]);
            }
                if (!$token = auth()->attempt(['mobile' => $mobile, 'password' => $password])) {
                    return response()->json([
                        'errors' => [
                            'message' => 'Unauthorized'
                        ]
                    ], 401);
                }
                if ($mobileOtp == null){
                    return response()->json([
                        'errors' => [
                            'message' => 'Invalid Code!'
                        ]
                    ], 401);
                }
                return response()->json([
                    'data' => [
                        'message' => 'User registered successfully',
                        'user' => $this->createNewToken($token)->original
                    ]
                ], 200);


            }
              else {
                return response()->json([
                    'errors' => [
                        'message' =>'You must provide email or mobile!'
                    ]
                ], 422);
            }

        }



    protected function createNewToken($token){
        return response()->json([
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60,
                'user' => auth()->user()
            ]
        ]);
    }

}
