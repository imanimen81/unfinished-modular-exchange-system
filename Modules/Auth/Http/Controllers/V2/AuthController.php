<?php

namespace Modules\Auth\Http\Controllers\V2;

use App\Http\Traits\Logs;
use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Entities\EmailOtp;
use Modules\Auth\Entities\Level;
use Modules\Auth\Entities\MobileOtp;
use Modules\Auth\Http\Requests\ChangePasswordRequest as ChangePasswordRequest;
use Modules\Auth\Http\Requests\LoginRequest as LoginRequest;
use Modules\Auth\Http\Requests\OtpRegisterRequest;
use Modules\Auth\Http\Requests\OtpRequest as OtpRequest;
use Modules\Auth\Http\Requests\RegisterRequest as RegisterRequest;
use Modules\Auth\Jobs\SendMobileOtpJob;
use Modules\Auth\Jobs\SendOtpEmailJob;
use Modules\Auth\Rules\Phone;

class AuthController extends Controller
{

    use Logs;
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'sendCode', 'register', 'authUser']]);
    }


    // login

    public function login(LoginRequest $request)
    {
        if (!$token = auth()->attempt($request->all())){
            $this->addLog($request, 'failed login',  null, User::class, User::class, Log::STATUS_FAILED);
            return response()->json([
                'errors' =>[
                    'message' => trans('auth.failed')
                ]
            ], 401);
        }
        // create token
        $this->addLog($request, 'succeed login',  auth()->user()->id, User::class, User::class, Log::STATUS_SUCCESS);
        return  $this->createNewToken($token)->original;
    }

    public function register(RegisterRequest $request)
    {
        $user = User::query()->create(
            [
                'email' => $request->get('email'),
                'mobile' => $request->get('mobile'),
                'password' => Hash::make($request->get('password')),
                'level_id' => Level::query()->where('level', 1)->first()->id,
                'status' => User::STATUS_NEED_TO_VERIFY,
            ]
        );
        if (!$token = auth()->attempt($request->all())){
            $this->addLog($request, 'failed register',  null, User::class, User::class, Log::STATUS_FAILED);
            return response()->json([
                'errors' =>
                    [
                        'message' => trans('auth.failed')
                    ]
            ], 401);
        }
        $this->addLog($request, 'succeed register',  auth()->user()->id, User::class, User::class, Log::STATUS_SUCCESS);
        return $this->createNewToken($token)->original;
    }

    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully']);
    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    public function userProfile() {
        return response()->json(auth()->user());
    }

    protected function createNewToken($token){
        return response()->json([
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60,
                'user' => auth()->user()
            ],
        ]);
    }

    protected function loginAfterRegister(Request $request)
    {
        try {
            if ($token = auth()->attempt($request->all())) {
                return response()->json([
                    'token' => $this->createNewToken($token)->original
                ], 401);
            }
            else {
                return response()->json([
                    'message' => 'Invalid Credentials'
                ], 401);
            }

        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 422);
        }

    }

    // change user password
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        if (Hash::check($request->get('old_password'), $user->password)) {
            $user->password = Hash::make($request->get('new_password'));
            $user->save();
            return response()->json([
                'message' => 'Password changed successfully'
            ], 200);
        }
        else {
            return response()->json([
                'errors' => [
                    'message' => 'Old Password is incorrect'
                ]
            ], 422);
        }
    }

    // login and register together
    public function sendCode(OtpRegisterRequest $request)
    {
        try {
            $mobile = $request->get('mobile');
            $email = $request->get('email');
            if ($request->has('email') && $request->has('mobile')) {
                return response()->json([
                    'errors' => [
                        'message' => 'Please enter either email or mobile'
                    ]
                ], 422);
            }
            if ($request->has('mobile')) {
                $type = MobileOtp::TYPE_LOGIN;
                $sendMobileOtp = MobileOtp::sentOtp($mobile, true, $type);
                dispatch(new SendMobileOtpJob($mobile, $sendMobileOtp));
                return response()->json([
                    'message' => 'Please check your phone to verify your account',
                    'code' => $sendMobileOtp
                ], 200);
            } elseif ($request->has('email')) {
                $type = EmailOtp::TYPE_LOGIN;
                $sendOtpEmail = EmailOtp::sentOtp($email, true, $type);
                dispatch(new SendOtpEmailJob($email, $sendOtpEmail));
                return response()->json([
                    'message' => 'Please check your email to verify your account',
                    'code' => $sendOtpEmail
                ], 200);
            } else {
                return response()->json([
                    'errors' => [
                        'message' => 'Please enter either mobile or email'
                    ]
                ], 422);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 422);
        }

    }

    public function authUser(OtpRegisterRequest $request)
    {
        try {
            $mobile = $request->get('mobile');
            $email = $request->get('email');
            if ($request->has('email') && $request->has('mobile')) {
                return response()->json([
                    'errors' => [
                        'message' => 'Please enter either email or mobile'
                    ]
                ], 422);
            }
            if ($request->has('email')){
                $checkCode = EmailOtp::checkCode($email, $request->get('code'));
                if ($checkCode){
                    $checkUser = User::query()->where('email', $email)->first();
                    if ($checkUser){
                        if (!$token = auth()->attempt($request->only('email', 'password'))){
                            $this->addLog($request, 'failed register',  null, User::class, User::class, Log::STATUS_FAILED);
                            return response()->json([
                                'errors' =>
                                    [
                                        'message' => trans('auth.failed')
                                    ]
                            ], 401);
                        }
                        return $this->createNewToken($token)->original;
                    }
                    if (!$checkUser){
                        User::query()->create([
                            'email' => $email,
                            'password' => Hash::make($request->get('password')),
                            'level_id' => Level::query()->where('level', 1)->first()->id,
                            'status' => User::STATUS_NEED_TO_VERIFY
                        ]);
                        if (!$token = auth()->attempt($request->only('email', 'password'))){
                            $this->addLog($request, 'failed register',  null, User::class, User::class, Log::STATUS_FAILED);
                            return response()->json([
                                'errors' =>
                                    [
                                        'message' => trans('auth.failed')
                                    ]
                            ], 401);
                        }
                    }
                    return $this->createNewToken(auth()->attempt($request->only('email', 'password')))->original;
                }
                return response()->json([
                    'errors' => [
                        'message' => 'کد وارد شده صحیح نمی باشد',
                        'code' => EmailOtp::countOtp($mobile)

                    ]
                ], 422);
            }
            if ($request->has('mobile')){
                $checkCode = MobileOtp::checkCode($mobile, $request->get('code'));
                if ($checkCode){
                    $checkUser = User::query()->where('mobile', $mobile)->first();
                    if ($checkUser) {
                        if (!$token = auth()->attempt($request->only('mobile', 'password'))){
                            $this->addLog($request, 'failed register',  null, User::class, User::class, Log::STATUS_FAILED);
                            return response()->json([
                                'errors' =>
                                    [
                                        'message' => trans('auth.failed')
                                    ]
                            ], 401);
                        }
                        return $this->createNewToken($token)->original;
                    }
                    if (!$checkUser){
                        User::query()->create([
                            'mobile' => $mobile,
                            'password' => Hash::make($request->get('password')),
                            'level_id' => Level::query()->where('level', 1)->first()->id,
                            'status' => User::STATUS_NEED_TO_VERIFY
                        ]);
                        if (!$token = auth()->attempt($request->only('mobile', 'password'))){
                            $this->addLog($request, 'failed register',  null, User::class, User::class, Log::STATUS_FAILED);
                            return response()->json([
                                'errors' =>
                                    [
                                        'message' => trans('auth.failed')
                                    ]
                            ], 401);
                        }
                        return response()->json([
                            'errors' => [
                                'message' => trans('auth.failed')
                            ]
                        ]);
                    }
                    return $this->createNewToken($token)->original;
                }
                return response()->json([
                    'errors' => [
                        'message' => 'کد وارد شده صحیح نمی باشد',
                        'code' => MobileOtp::countOtp($mobile)
                    ]
                ], 422);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 422);
        }
    }
}
