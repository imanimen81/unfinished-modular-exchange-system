<?php

namespace Modules\Auth\Http\Controllers\V2;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Auth\Entities\State;

class StateController extends Controller
{

    // all states
    public function allStates(){
        return State::all();
    }

    // get cities by state_id
    public function getCitiesByStateId($id){
        try {
            $state = State::findOrFail($id);
            return $state->cities;
        } catch (\Exception $e) {
            return response()->json([
                'errors' =>
                    [
                        'message' => $e->getMessage()
                    ]
            ]);
        }
    }

}
