<?php

namespace Modules\Auth\Http\Controllers\V2;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Entities\EmailOtp;
use Modules\Auth\Entities\MobileOtp;
use Modules\Auth\Jobs\SendMobileOtpJob;
use Modules\Auth\Jobs\SendOtpEmailJob;

class OtpController extends Controller
{

    // resend sms otp
    public function resendSmsOtp(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'mobile' => 'required|numeric|digits:11',
            ]);
            $mobile = $request->get('mobile');
            if ($validator->fails()) {
                return response()->json([
                    'error' => [
                        'message' => $validator->errors()->first(),
                    ]
                ]);
            }
            $code = MobileOtp::sentOtp($mobile, true, $type = MobileOtp::TYPE_RESEND_SMS);
            dispatch(new SendMobileOtpJob($mobile, $code));
            return response()->json([
                'data' => [
                    'message' => 'Otp sent successfully',
                    'code' => $code
                    ]
                ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }


    // resend email otp
    public function resendEmailOtp(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);
            $email = $request->get('email');
            if ($validator->fails()) {
                return response()->json([
                    'error' => [
                        'message' => $validator->errors()->first(),
                    ]
                ]);
            }
            $code = EmailOtp::sentOtp($email, true, $type = EmailOtp::TYPE_RESEND_EMAIL);
            dispatch(new SendOtpEmailJob($email, $code));
            return response()->json([
                'data' => [
                    'message' => 'Otp sent successfully',
                    'code' => $code
                    ]
                ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

}
