<?php

namespace Modules\Auth\Http\Controllers\V2;

use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class CheckUserController extends Controller
{
    const REGISTERED = 0;
    const UNREGISTERED = 1;

    public function checkFields(Request $request)
    {
        $email = $request->get('email');
        $mobile = $request->get('mobile');
        if($request->has('mobile') and $request->has('email')){
            return response()->json([
                'error' => [
                    'message' =>'You can not use both email and mobile'
                ]
            ], 422);
        }

        if ($request->has('mobile')){
            $mobileSerach = User::query()->where('mobile', $mobile)->first();
            if($mobileSerach){
                return response()->json([
                    'data' => [
                        'new_uesr' => self::REGISTERED,
                        'message' => 'User with this mobile already exists!'
                    ]
                ], 200);
            }
            if ($mobileSerach == null){
                return response()->json([
                    'data' => [
                        'new_uesr' => self::UNREGISTERED,
                        'message' => 'User with this mobile does not exist!'
                    ]
                ], 200);
            }
        }

        if($request->has('email')){
            $emailSerach = User::query()->where('email', $email)->first();
            if($emailSerach){
                return response()->json([
                    'data' => [
                        'new_uesr' => self::REGISTERED,
                        'message' => 'User with this email already exists!'
                    ]
                ], 200);
            }
            if ($emailSerach == null){
                return response()->json([
                    'data' => [
                        'new_uesr' => self::UNREGISTERED,
                        'message' => 'User with this email does not exist!'
                    ]
                ], 200);
            }
        }
        else {
            return response()->json([
                'error' => [
                    'message' =>'You must enter email or mobile'
                ]
            ], 422);
        }
    }
}
