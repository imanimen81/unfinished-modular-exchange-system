<?php

namespace Modules\Auth\Http\Controllers\V2;

use App\Http\Traits\Logs;
use App\Models\Log;
use App\Models\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Modules\Auth\Emails\EmailOtpVerification;
use Modules\Auth\Entities\EmailOtp;
use Modules\Auth\Entities\MobileOtp;
use Modules\Auth\Http\Requests\CheckPassword;
use Modules\Auth\Http\Requests\LoginRequest as LoginRequest;
use Modules\Auth\Http\Requests\SendOtpRequest;
use Modules\Auth\Jobs\SendMobileOtpJob;
use Modules\Auth\Jobs\SendOtpEmailJob;
use PharIo\Manifest\Email;

class OtpLoginController extends Controller
{
    use Logs;
    // login
    public function otpLogin(SendOtpRequest $request)
    {
        $email = $request->get('email');
        $mobile = $request->get('mobile');

        if ($request->has('email') and $request->has('mobile')) {
            return response()->json([
                'errors' => [
                    'message' => 'You can not use both email and mobile'
                ]
            ], 422);
        }

        if ($request->has('mobile')){
            $type = MobileOtp::TYPE_LOGIN;
            $sendMobileOtp = MobileOtp::sentOtp($mobile, true, $type);
            if ($sendMobileOtp){
                dispatch(new SendMobileOtpJob($mobile, $sendMobileOtp));
                return response()->json([
                    'data' => [
                        'message' => 'Otp sent successfully',
                        'code' => $sendMobileOtp // todo: change to api call response
                    ]
                ], 200);
            }
            if ($sendMobileOtp == null){
                return response()->json([
                    'errors' => [
                        'message' => 'Otp not sent'
                    ]
                ], 200);
            }
        }

        if($request->has('email')){
            $type = EmailOtp::TYPE_LOGIN;
            $sendEmailOtp = EmailOtp::sentOtp($email, true, $type);
            if ($sendEmailOtp){
                dispatch(new SendOtpEmailJob($email, $sendEmailOtp));
                return response()->json([
                    'data' => [
                        'message' => 'Email Sent. Check Your Inbox!',
                        'code' => $sendEmailOtp // todo: change to api call response
                    ]
                ], 200);
            }
            return response()->json([
                'data' => [
                    'message' => 'Email Sent. Check Your Inbox!'
                ]
            ], 200);
        }
        else {
            return response()->json([
                'errors' => [
                    'message' =>'You must provide email or mobile!'
                ]
            ], 422);
        }
    }


    // verify and login
    public function otpVerify(LoginRequest $request)
    {
        $mobile = $request->get('mobile');
        if ($request->has('mobile')){
            $checkMobile = MobileOtp::checkCode($mobile, $request->get('code'));
            if ($checkMobile){
                if (!$token = auth()->attempt($request->only(['mobile', 'password']))) {
                    $this->addLog($request, 'failed login', null, null, null, Log::STATUS_FAILED);
                    return response()->json([
                        'errors' => [
                            'message' => trans('auth.failed')
                        ]
                    ], 422);
                }
                $this->addLog($request, 'succeed login', auth()->user()->id, User::class, User::class, Log::STATUS_SUCCESS);
                return $this->createNewToken($token)->original;
            }
            if (!$checkMobile) {
                return response()->json([
                    'errors' => [
                        'message' => 'Invalid code',
                        'count' => MobileOtp::countOtp($mobile)
                    ]
                ], 422);
            }

            else {
                return response()->json([
                    'errors' => [
                        'message' => trans('auth.failed')
                    ]
                ], 422);
            }
        }

        $email = $request->get('email');
        if ($request->has('email')){
            $checkEmail = EmailOtp::checkCode($email, $request->get('code'));
            if ($checkEmail){
                if (!$token = auth()->attempt($request->only(['email', 'password']))) {
                    $this->addLog($request, 'failed login', null, null, null, Log::STATUS_FAILED);
                    return response()->json([
                        'errors' => [
                            'message' => trans('auth.failed')
                        ]
                    ], 422);
                }
                $this->addLog($request, 'succeed login', auth()->user()->id, User::class, User::class, Log::STATUS_SUCCESS);
                return $this->createNewToken($token)->original;
            }
            if (!$checkEmail) {
                return response()->json([
                    'errors' => [
                        'message' => 'Invalid code',
                        'count' => MobileOtp::countOtp($mobile)
                    ]
                ], 422);
            }
             else {
                 return response()->json([
                    'errors' => [
                        'message' => trans('auth.failed')
                    ]
                ], 422);
             }
        }

    }


    protected function createNewToken($token){
        return response()->json([
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60,
                'user' => auth()->user()
            ]
        ]);
    }

    public function checkPass(CheckPassword $request)
    {
        if ($request->has('email')){
            $c = User::query()->where('email', $request->get('email'))->first();
            if ($c){
                if (Hash::check($request->get('password'), $c->password)){
                    return response()->json(
                        true,
                        200);
                }
                else {
                    return response()->json(
                        false,
                    422);
                }
            }
        }
        if ($request->has('mobile')){
            $c = User::query()->where('mobile', $request->get('mobile'))->first();
            if ($c){
                if (Hash::check($request->get('password'), $c->password)){
                    return response()->json(
                        true,
                        200);
                }
                else {
                    return response()->json(
                        false,
                    422);
                }
            }
        }
        else {
            return response()->json([
                    'errors' => [
                        'message' => 'ایمیل یا شماره موبایل را وارد کنید'
                    ],
            ],422);
        }

    }
}
