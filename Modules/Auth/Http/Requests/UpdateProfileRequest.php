<?php

namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Auth\Rules\CheckYear;
use Modules\Auth\Rules\Phone;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone' => 'sometimes|numeric|unique:profiles,phone',
            'mobile' => ['required','numeric', new Phone],
            'address' => 'required|string|max:255',
            'national_code' => 'required|numeric|unique:profiles,national_code',
            'birth_date' => 'required|date', new CheckYear(),
            'post_code' => 'required|numeric|unique:profiles,post_code',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
