<?php

namespace Modules\Auth\Http\Services;

class kavenegar
{

    public static function sendCode($mobile, $code){
        // set post fields
        try {
            $post = [
                'receptor' => $mobile,
                'token' => $code,
                'template' => 'newUser',
            ];

            $ch = curl_init('https://api.kavenegar.com/v1/'.env('KAVENEGAR_TOKEN').'/verify/lookup.json');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

            $response = curl_exec($ch);
            curl_close($ch);
            return json_decode($response);
        } catch (\Exception $e){
            return response()->json([
                'status' => 'error', 'message' => $e->getMessage()
            ], 500);
        }
    }
}
