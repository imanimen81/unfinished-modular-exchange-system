<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
       id="m_-2336860179533608693bodyTable"
       style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#f5f5f5">
    <tbody>
    <tr>
        <td align="center" valign="top" id="m_-2336860179533608693bodyCell"
            style="height:100%;margin:0;padding:10px;width:100%;border-top:0">


            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                   style="border-collapse:collapse;border:0;max-width:600px!important">
                <tbody>
                <tr>
                    <td valign="top" id="m_-2336860179533608693templatePreheader"
                        style="background-color:#f5f5f5;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="min-width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td valign="top" style="padding-top:9px">


                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                           style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                                        <tbody>
                                        <tr>

                                            <td align="top"
                                                style="padding:0px 18px 9px;text-align:right;word-break:break-word;color:#777777;font-family:Helvetica;font-size:12px;line-height:150%">

                                                <div dir="rtl"><span
                                                        style="font-family:'IRANSans','Iranian Sans',Tahoma,Arial,sans-serif!important">کامکس ، پلتفرم معاملات ارز دیجیتال</span>
                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>


                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" id="m_-2336860179533608693templateHeader"
                        style="background-color:#ffffff;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="min-width:100%;min-height:54px;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td valign="top" style="padding:9px">
                                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0"
                                           style="min-width:100%;border-collapse:collapse">
                                        <tbody>
                                        <tr>
                                            <td valign="top"
                                                style="width:45px;padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:left">


                                                <a style="text-decoration:none!important" href="https://kamex.ir"
                                                   target="_blank">
                                                    <img align="center" alt="" src="http://kamex.ir/api/kamex.png"
                                                         width="36"
                                                         style="padding-bottom:0;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none"
                                                         class="CToWUd">
                                                </a>
                                            </td>
                                            <td valign="top"
                                                style="padding-right:9px;padding-left:9px;padding-top:0;padding-bottom:0;text-align:right">

                                                <table align="right" width="100%" border="0" cellpadding="0"
                                                       cellspacing="0" style="min-width:100%;border-collapse:collapse">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <a href="#"
                                                               style="color:#777!important;text-decoration:none;font-family:'IRANSans','Iranian Sans',Tahoma,Arial,sans-serif!important"
                                                               target="_blank"
                                                               data-saferedirecturl="https://www.google.com/url?q=https://kamex.ir/profile/vahid.rezazadeh&amp;source=gmail&amp;ust=1604159283798000&amp;usg=AFQjCNGb6F7YNRsgAm8xg_Bt6Fl8VscxWw">
                                                                <span
                                                                    style="padding:0 8px;font-size:13px;color:#777!important">کد اعتبار سنجی </span>
                                                            </a>
                                                        </td>

                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="min-width:100%;border-collapse:collapse;table-layout:fixed!important">
                            <tbody>
                            <tr>
                                <td style="min-width:100%;padding:0px 18px">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                           style="min-width:100%;border-top-width:1px;border-top-style:solid;border-top-color:#669900;border-collapse:collapse">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <span></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" id="m_-2336860179533608693templateBody"
                        style="background-color:#ffffff;border-top:0;border-bottom:2px solid #eaeaea;padding-top:0;padding-bottom:9px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="min-width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td valign="top" style="padding-top:9px">


                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                           style="max-width:100%;min-width:100%" width="100%">
                                        <tbody>
                                        <tr>

                                            <td valign="top"
                                                style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px">

                                                <div dir="rtl"
                                                     style="font-family:'IRANSans','Iranian Sans',Tahoma,Arial,sans-serif!important;text-align:right">
                                                    <br>
                                                    کد اعتبار سنجی شما در کامکس :
                                                    <strong>{{$code}}</strong>
                                                    <br>
                                                    <br>
                                                    {{--<div style="direction:ltr;text-align:left">
                                                        <a href="https://kamex.ir/reset-password/vahid.rezazadeh_744a6174556c361e4e9ec68660df632e" style="text-decoration:none!important;color:#777!important" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://kamex.ir/reset-password/vahid.rezazadeh_744a6174556c361e4e9ec68660df632e&amp;source=gmail&amp;ust=1604159283798000&amp;usg=AFQjCNFI5pZPHv3QhpTk3P2BhGynVRJ56g">
                                                            https://kamex.ir/reset-<wbr>password/vahid.rezazadeh_<wbr>744a6174556c361e4e9ec68660df63<wbr>2e
                                                        </a>
                                                    </div>--}}
                                                    <br>
                                                    <div style="text-align:center">
                                                        <a style="padding:7px 12px;margin:8px 0px;text-decoration:none!important;font-family:'IRANSans','Iranian Sans',Tahoma,Arial,sans-serif!important;background-color:#02483c;color:white;border-radius:5px;text-align:center"
                                                           href="https://kamex.ir/" target="_blank">ورود به سایت</a>
                                                    </div>
                                                    <br>
                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>


                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" id="m_-2336860179533608693templateFooter"
                        style="background-color:#f5f5f5;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                               style="min-width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td valign="top" style="padding-top:9px">


                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                           style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%">
                                        <tbody>
                                        <tr>

                                            <td valign="top"
                                                style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#777777;font-family:'IRANSans','Iranian Sans',Tahoma,Arial,sans-serif!important;font-size:12px;line-height:150%;text-align:center">
                                                <div style="text-align:center;direction:rtl">
                                                    این ایمیل برای <a href="mailto:{{$email}}"
                                                                      target="_blank">{{$email}}</a> ارسال شده است.
                                                </div>

                                                <div style="margin:7px">
                                                    <a style="text-decoration:none!important"
                                                       href="https://www.linkedin.com/in/kamex-company-541b8a1b7/"
                                                       target="_blank"><img
                                                            src="https://ci5.googleusercontent.com/proxy/I5OlI-G_NLfGNh4TLPKO8BuprWFzHbBqYfX9T8KFnJI73BBVjcI_j2KDJvVwuKW4Rzi5U8b3nlIt_uRyKY-rABln-U6s2Q=s0-d-e1-ft#https://ponisha.ir/asset/v1/img/social/facebook.png"
                                                            width="18" class="CToWUd"></a>
                                                    <a href="https://t.me/kamex_exchange/"
                                                       style="padding:0 12px;text-decoration:none!important"
                                                       target="_blank"><img
                                                            src="https://ci4.googleusercontent.com/proxy/2im_Dskc0rX83Nvs8d1AmwqgPOlOLRTimKGbAMlrc_NZlkXGZBSZ1nLeDFqXfARjNCFibZh4fH1gsi9yX2mX90LJGuMzOcE=s0-d-e1-ft#https://ponisha.ir/asset/v1/img/social/instagram.png"
                                                            width="18" class="CToWUd"></a>
                                                </div>
                                                <div dir="rtl">&nbsp;© کامکس <a href="https://kamex.ir/terms"
                                                                                style="text-decoration:none!important;color:#777777;font-weight:normal;text-decoration:underline"
                                                                                target="_blank">قوانین</a> <a
                                                        href="https://kamex.ir/help"
                                                        style="color:#777777;font-weight:normal;text-decoration:none!important"
                                                        target="_blank">راهنما</a><br>

                                                    <br>
                                                    <br>
                                                    &nbsp;
                                                </div>

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>


                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>


        </td>
    </tr>
    </tbody>
</table>

