<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Carbon\Carbon;

class EmailOtp extends Model
{
    use HasFactory;

    protected $table = 'email_otps';
    protected $fillable = [
        'email',
        'code',
        'count',
        'expire_date',
        'type'
    ];

    const TYPE_LOGIN = 0;
    const TYPE_REGISTER = 1;
    const TYPE_FORGOT_PASSWORD = 2;
    const TYPE_CHANGE_EMAIL = 3;
    const TYPE_RESEND_EMAIL = 4;
    const TYPE_RESEND_SMS = 5;


    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\EmailOtpFactory::new();
    }

    public static function sentOtp($email, $isEmail, $type)
    {
        $code = 0;
        if ($isEmail) {
            $code = rand(100000, 999999);
        }
        $otp = self::getOtp($email);
        if (!$otp) {
            $otp = self::create([
                'code' => $code,
                'count' => 3,
                'email' => $email,
                'type' => $type,
                'expire_date' => Carbon::now()
                    ->addMinutes(+5)
                    ->format('Y-m-d H:i'),
            ]);
        } else {
            $otp->code = $code;
            $otp->expire_date = Carbon::now()
                ->addMinutes(+5)
                ->format('Y-m-d H:i');
            $otp->save();
        }
//        dd($otp->code);
        return $otp->code;
    }

    public static function getOtp($address)
    {
        return self::where('email', $address)
            ->where('expire_date', '>=',
                date('Y-m-d H:i'))
            ->first();

    }

    public static function checkCode($email, $code)
    {
        $otp = self::getOtp($email);
        if (!$otp) {
            return false;
        }
        if ($otp->code != $code) {
            $otp->count--;
            $otp->save();
            return false;
        } else {
            $otp->expire_date = Carbon::now()
                ->addMinutes(10)
                ->format('Y-m-d H:i');
            $otp->save();
            $otp->delete();
        }
        return true;
    }

    public static function countOtp($mobile)
    {
        $otp = self::getOtp($mobile);
        return $otp->count ?? 0;
    }
}
