<?php

namespace Modules\Auth\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFile extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'selfie_image',
        'national_card_image',
        'expire_date',
        'is_confirmed',
        'reject_reason'
    ];
    protected $append = ['selfie_url', 'national_url'];
    protected $table = 'user_files';

    const IS_CONFIRMED = 1;
    const IS_NOT_CONFIRMED = 0;

    public function getNationalUrlAttribute()
    {
        return 'https://kamex.dyneemadev.com/'.$this->national_card_image;
    }

    public function getSelfieUrlAttribute(){
        return 'https://kamex.dyneemadev.com/'. $this->selfie_image;
    }

    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\UserFileFactory::new();
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
