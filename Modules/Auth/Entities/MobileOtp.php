<?php

namespace Modules\Auth\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Auth\Http\Services\kavenegar;

class MobileOtp extends Model
{
    use HasFactory;

    protected $table = 'mobile_otps';
    protected $fillable = [
        'mobile',
        'code',
        'count',
        'expire_date',
        'type'
    ];
    const TYPE_LOGIN = 0;
    const TYPE_REGISTER = 1;
    const TYPE_FORGOT_PASSWORD = 2;
    const TYPE_CHANGE_EMAIL = 3;
    const TYPE_RESEND_EMAIL = 4;
    const TYPE_RESEND_SMS = 5;
    const TYPE_UPDATE_PROFILE = 6;


    const TYPE = [
        'register' => 'register',
        'login' => 'login',
    ];

    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\MobileOtpFactory::new();
    }

    public static function sentOtp($mobile, $isMobile, $type)
    {
        $code = 0;
        if ($isMobile) {
            $code = rand(100000, 999999);
        }

        $otp = self::getOtp($mobile);
//        dd($otp->code);
        if (!$otp) {
            $otp = self::create([
                'code' => $code,
                'count' => 3,
                'mobile' => $mobile,
                'type' => $type,
                'expire_date' => Carbon::now()
                    ->addMinutes(+5)
                    ->format('Y-m-d H:i'),
            ]);
        } else {
            $otp->code = $code;
            $otp->expire_date = Carbon::now()
                ->addMinutes(+5)
                ->format('Y-m-d H:i');
            $otp->save();
        }
        return $otp->code;
    }

    public static function getOtp($address)
    {
        return self::where('mobile', $address)
            ->where('expire_date', '>=',
                date('Y-m-d H:i'))
            ->first();

    }

    public static function checkCode($mobile, $code)
    {
        $otp = self::getOtp($mobile);
        if (!$otp) {
            return false;
        }
        if ($otp->code != $code) {
            $otp->count--;
            $otp->save();
            return false;
        } else {
            $otp->expire_date = Carbon::now()
                ->addMinutes(10)
                ->format('Y-m-d H:i');
            $otp->save();
            $otp->delete();
        }
        return true;
    }

    // count filed in mobile_otps table
    public static function countOtp($mobile)
    {
        $otp = self::getOtp($mobile);
        return $otp->count ?? 0;
    }
}
