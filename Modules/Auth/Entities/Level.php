<?php

namespace Modules\Auth\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Level extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'level',
        'wage_buyer',
        'wage_seller',
        'max_trade',
        'max_trades_text'
    ];

    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\LevelFactory::new();
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
