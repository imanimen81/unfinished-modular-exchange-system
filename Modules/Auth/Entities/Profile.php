<?php

namespace Modules\Auth\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use HasFactory, SoftDeletes;

    protected $appends = ['user_profile'];
    protected $table = 'profiles';
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'national_code',
        'phone',
        'mobile',
        'city',
        'birth_date',
        'address',
        'post_code',
        'status',
        'avatar',
        'city'
    ];

    const STATUS = [
      1 => 'PENDING_OTP',
      2 => 'WAITING_FOR_VERIFICATION',
      3 => 'VERIFIED',
    ];
    const STATUS_PENDING = 1;
    const STATUS_WAITING_FOR_VERIFICATION = 2;
    const STATUS_VERIFIED = 3;

    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\ProfileFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserProfileAttribute()
    {
        try {
            $userId = auth()->user()->id;
            $user = User::query()->findOrFail($userId);

            $userFiles = $user->file;
            switch ($user->level_id){
                case 1:
                    return response()->json([
                            'email' => $user->email,
                            'mobile' => $user->mobile,
                            'status_label' => $user->status_label,
                            'user_level' => $user->level->level,
                    ])->original;
                    break;
                case 2:
                    $profileArray = [];
                    array_push($profileArray, $user->profile->first_name);
                    array_push($profileArray, $user->profile->last_name);
                    array_push($profileArray, $user->profile->mobile);
                    array_push($profileArray, $user->profile->national_code);
                    array_push($profileArray, $user->profile->birth_date);
                    array_push($profileArray, $user->email);
                    return response()->json([
                            'first_name' => $profileArray[0],
                            'last_name' => $profileArray[1],
                            'mobile' => $profileArray[2],
                            'national_code' => $profileArray[3],
                            'birth_date' => $profileArray[4],
                            'email' => $profileArray[5],
                            'status_label' => $user->status_label,
                             'user_level' => $user->level->level,
                            'files' => [
                                'selfie_image' => $userFiles->selfie_image,
                                'selfie_image_url' => $userFiles->selfie_url,
                                'national_card_image' => $userFiles->national_card_image,
                                'national_card_image_url' => $userFiles->national_url,
                            ]
                    ], 200)->original;
                    break;
                case 3;
                    $profileArray = [];
                    array_push($profileArray, $user->profile->first_name);
                    array_push($profileArray, $user->profile->last_name);
                    array_push($profileArray, $user->profile->mobile);
                    array_push($profileArray, $user->profile->national_code);
                    array_push($profileArray, $user->profile->birth_date);
                    array_push($profileArray, $user->profile->post_code);
                    array_push($profileArray, $user->profile->city);
                    array_push($profileArray, $user->profile->address);
                    array_push($profileArray, $user->email);
                    return response()->json([
                            'first_name' => $profileArray[0],
                            'last_name' => $profileArray[1],
                            'mobile' => $profileArray[2],
                            'national_code' => $profileArray[3],
                            'birth_date' => $profileArray[4],
                            'email' => $profileArray[7],
                            'post_code' => $profileArray[5],
                            'city' => $profileArray[6],
                            'address' => $profileArray[7],
                            'status_label' => $user->status_label,
                            'user_level' => $user->level->title,
//                            'files' => [
//                                'selfie_image' => $userFiles->selfie_image,
//                                'selfie_image_url' => $userFiles->selfie_url,
//                                'national_card_image' => $userFiles->national_card_image,
//                                'national_card_image_url' => $userFiles->national_url,
//                            ]
                    ], 200)->original;

            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 400);
        }
    }
}
