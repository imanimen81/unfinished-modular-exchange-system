<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'state_id',
        'enname',
        'faname',
    ];
    protected $table = 'cities';


    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\CityFactory::new();
    }


    public function state(){
        return $this->belongsTo(State::class, 'state_id');
    }
}
