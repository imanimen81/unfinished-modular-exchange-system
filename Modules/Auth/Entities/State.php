<?php

namespace Modules\Auth\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class State extends Model
{
    use HasFactory;

    protected $fillable = [
        'enname',
        'faname',
    ];
    protected $table = 'states';

    protected static function newFactory()
    {
        return \Modules\Auth\Database\factories\StateFactory::new();
    }

    public function cities(){
        return $this->hasMany(City::class);
    }
}
