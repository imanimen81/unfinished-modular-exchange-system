<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Modules\Auth\Http\Controllers\V2\AuthController;
use Modules\Auth\Http\Controllers\V2\CheckUserController;
use Modules\Auth\Http\Controllers\V2\OtpLoginController;
use Modules\Auth\Http\Controllers\V2\OtpRegisterController;
use Modules\Auth\Http\Controllers\V2\ProfileController;
use Modules\Auth\Http\Controllers\V2\OtpController;
use Modules\Auth\Http\Controllers\V2\UploadUserFilesController;
use Modules\Auth\Http\Controllers\V2\StateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v2/auth'], function ($router){

    // check user details
    $router->post('/check-user', [CheckUserController::class, 'checkFields']);

    // default login and register
    $router->post('/login', [AuthController::class, 'login']);
    $router->post('/register', [AuthController::class, 'register']);

    // logout
    $router->get('/logout', [AuthController::class, 'logout']);
    $router->get('/refresh', [AuthController::class, 'refresh']);

    // change password
    $router->post('/change-password', [AuthController::class, 'changePassword']);

    // otp login
    $router->post('/login/otp-send', [OtpLoginController::class, 'otpLogin']);
    $router->post('/login/check-password', [OtpLoginController::class, 'checkPass']);
    $router->post('/login/otp-verify', [OtpLoginController::class, 'otpVerify']);

    // opt register
    $router->post('/register/otp-send', [OtpRegisterController::class, 'otpRegister']);
    $router->post('/register/otp-verify', [OtpRegisterController::class, 'otpRegisterVerify']);

    // upload user files
    $router->post('/profile/upload-files', [UploadUserFilesController::class, 'uploadFiles']);

    // get user profile
    $router->get('/profile', [ProfileController::class, 'getProfile']);

    // resend codes
    $router->post('/otp/resend-sms', [OtpController::class, 'resendSmsOtp']);
    $router->post('/otp/resend-email', [OtpController::class, 'resendEmailOtp']);

    $router->post('/profile/update-level-two', [ProfileController::class, 'updateUserLevel2']);
    $router->post('/profile/confirm-update', [ProfileController::class, 'verifyUserLevel2']);
    $router->get('/profile/get-user-profile', [ProfileController::class, 'getUserProfileLevel2']);


    $router->post('/profile/update-level-three', [ProfileController::class, 'updateLevelThree']);
    $router->post('/profile/confirm-update-level-three', [ProfileController::class, 'verifyLevelThree']);

    // cities and states
    $router->get('/states', [StateController::class, 'allStates']);
    $router->post('/cities/{id}', [StateController::class, 'getCitiesByStateId']);

    // main auth
    $router->post('/send-code', [AuthController::class, 'sendCode']);
    $router->post('/auth-user', [AuthController::class,'authUser']);
});

