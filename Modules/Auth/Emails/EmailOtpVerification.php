<?php

namespace Modules\Auth\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailOtpVerification extends Mailable
{
    use Queueable, SerializesModels;

    private $email, $code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $code)
    {
        $this->email = $email;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(
            env('MAIL_FROM_ADDRESS'),
            env('MAIL_FROM_NAME')
        )
            ->to($this->email)
            ->view('auth::emails.otp')
            ->with([
                'title' => 'کد اعتبار سنجی',
                'code' => $this->code,
                'email' => $this->email
            ]);
    }
}
