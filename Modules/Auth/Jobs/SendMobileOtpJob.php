<?php

namespace Modules\Auth\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Http\JsonResponse;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Auth\Entities\MobileOtp;
use Modules\Auth\Http\Services\kavenegar;

class SendMobileOtpJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $mobilem, $code;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mobile, $code)
    {
        $this->mobile = $mobile;
        $this->code = $code;
        $this->queue = 'send_mobile_otp';
    }


    /**
     * Execute the job.
     *
     * @return JsonResponse
     */
    public function handle()
    {
        try {
            kavenegar::sendCode($this->mobile, $this->type);
        } catch (\Exception $e) {
             return response()->json([
                'errors' =>  [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }
}
