<?php

namespace Modules\Auth\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Modules\Auth\Emails\EmailOtpVerification;

class SendOtpEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $code, $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $code)
    {
        $this->code = $code;
        $this->email = $email;
        $this->queue = 'send_otp_email';
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle()
    {
        try {
            $otpMail = new EmailOtpVerification($this->email, $this->code);
            Mail::to($this->email)->send($otpMail);
        } catch(\Exception $e){
            return response()->json([
                'error' =>  [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }
}
