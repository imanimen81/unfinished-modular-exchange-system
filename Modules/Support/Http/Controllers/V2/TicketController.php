<?php

namespace Modules\Support\Http\Controllers\V2;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Support\Entities\Conversation;
use Modules\Support\Entities\Department;
use Modules\Support\Entities\Ticket;
use Modules\Support\Http\Requests\OpenTicketRequest as OpenTicketRequest;
use Modules\Support\Http\Traits\TicketData;

class TicketController extends Controller
{
    use TicketData;
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function openTicket(OpenTicketRequest $request){
        try {
            $department = Department::getDepartment($request);
            $ticket = Ticket::query()->create([
                'creator_id' => auth()->user()->id,
                'creator_type' => User::class,
                'department_id' => $department->id,
                'status' => Ticket::STATUS_OPEN,
                'priority' => $request->get('priority'),
                'owner_id' => auth()->user()->id,
                'owner_type' => User::class,
            ]);
            $ticketMessage = Conversation::query()->create([
                'ticket_id' => $ticket->id,
                'reply_id' => auth()->user()->id,
                'reply_type' => User::class,
                'message' => $request->get('message'),
                'subject' => $request->get('subject'),
            ]);
            return response()->json([
                'message' => 'Ticket opened successfully',
                'data' => [
                    'ticket' => $ticket,
                    'ticket_conversation' => $ticketMessage,
                ],
            ]);
        }  catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    // get ticket conversation
    public function ticketConversation(Request $request){
        try {
            $ticket = $this->getTicket($request);
            $conversation = $ticket->conversations;
            return response()->json([
                'data' => [
                    'ticket_id' => $ticket->id,
                    'conversation' => $conversation,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    // user reply to reply
    public function replyConversation(Request $request){
        try {
            $conv = $this->getConversation($request);
            $conversation = Conversation::query()->create([
                'ticket_id' => $conv->ticket_id,
                'reply_id' => auth()->user()->id,
                'reply_type' => User::class,
                'message' => $request->get('message'),
                'subject' => $conv->subject,
                'ticket_conversation_id' => $conv->id,
            ]);
            $ticket = $conv->ticket;
            $ticket->update([
                'status' => Ticket::STATUS_OPEN,
            ]);
            return response()->json([
                'message' => 'Ticket replied successfully',
                'data' => [
                    'ticket_conversation' => $conversation,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function closeTicket(Request $request){
        try {
            $ticket = $this->getTicket($request);
            $ticket->update(['status' => Ticket::STATUS_CLOSED]);
            return response()->json([
                'message' => 'Ticket closed successfully',
                'data' => [
                    'ticket_id' => $ticket->id,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    public function getTickets(){
        try {
            $user = auth()->user();
            $tickets = $user->tickets;
            return response()->json([
                'data' => [
                    'tickets' => $tickets,
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    public function showTicket(Request $request, $id)
    {
        try {
            $ticket = Ticket::query()->where('creator_id', auth()->user()->id)->findOrFail($id);
            if ($ticket){
                $ticket->conversations;
            } else {
                return response()->json([
                    'errors' => [
                        'message' => 'Ticket not found',
                    ]
                ]);
            }
            return response()->json([
                'data' => [
                    'ticket' => $ticket,
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function ticketListStatus(){
        try {
            $user = auth()->user();
            $tickets = $this->listTicketsByStatus();
            return $tickets;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    public function ticketListPriority(){
        try {
            $user = auth()->user();
            $tickets = $this->ListTicketsByPriority();
            return $tickets;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }


    public function ticketListDepartments(Request $request){
        try {
            $department = Department::getDepartment($request);
            $tickets = $this->getTicketsByDepartment($department->id);
            return response()->json([
                'data' => $tickets,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    public function replyTicket(Request $request){
        try {
            $user = auth()->user();
            $ticket = $this->getTicket($request);
            $reply = Conversation::query()->create([
                'ticket_id' => $ticket->id,
                'reply_id' => auth()->user()->id,
                'reply_type' => User::class,
                'message' => $request->get('message'),
                'subject' => $ticket->conversations->last()->subject,
            ]);
            if($ticket->status == Ticket::STATUS_CLOSED){
                $ticket->update([
                    'status' => Ticket::STATUS_USER_REPLY,
                ]);
            }
            $ticket->update([
                'status' => Ticket::STATUS_USER_REPLY,
            ]);
            return response()->json([
                'message' => 'Ticket replied successfully',
                'data' => [
                    'ticket_conversation' => $reply,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    protected function getTicket(Request $request){
        return Ticket::query()
            ->where('creator_id', auth()->user()->id)
            ->where('id',$request->get('ticket_id'))
            ->firstOrFail();
    }

    protected function getTicketsByDepartment($department_id){
        return Ticket::query()
            ->where('creator_id', auth()->user()->id)
            ->where('department_id', $department_id)
            ->get();
    }

    protected function getConversation(Request $request){
        return Conversation::query()
            ->where('reply_id', auth()->user()->id)
            ->where('id',$request->get('conversation_id'))
            ->firstOrFail();
    }

}
