<?php

namespace Modules\Support\Http\Traits;

use Modules\Support\Entities\Ticket;

trait TicketData
{
    public function listTicketsByStatus(){
        $tickets = Ticket::query();
        $tickets->when(request('filter_status') == Ticket::STATUS_OPEN, function ($query) {
            $query->where('creator_id', auth()->user()->id)->where('status', Ticket::STATUS_OPEN);
        });
        $tickets->when(request('filter_status') == Ticket::STATUS_CLOSED, function ($query) {
            $query->where('creator_id', auth()->user()->id)->where('status', Ticket::STATUS_CLOSED);
        });

        $tickets->when(request('filter_status') == Ticket::STATUS_USER_REPLY, function ($query){
           $query->where('creator_id', auth()->user()->id)->where('status', Ticket::STATUS_USER_REPLY);
        });

        $data = $tickets->latest()->paginate(15);
        return $data;
    }


    public function ListTicketsByPriority(){
        $tickets = Ticket::query();
        $tickets->when(request('filter_priority') == Ticket::PRIORITY_LOW, function ($query) {
            $query->where('creator_id', auth()->user()->id)->where('priority', Ticket::PRIORITY_LOW);
        });
        $tickets->when(request('filter_priority') == Ticket::PRIORITY_MEDIUM, function ($query) {
            $query->where('creator_id', auth()->user()->id)->where('priority', Ticket::PRIORITY_MEDIUM);
        });
        $tickets->when(request('filter_priority') == Ticket::PRIORITY_HIGH, function ($query) {
            $query->where('creator_id', auth()->user()->id)->where('priority', Ticket::PRIORITY_HIGH);
        });

        $data = $tickets->latest()->paginate(15);
        return $data;
    }
}
