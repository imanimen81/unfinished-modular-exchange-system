<?php

namespace Modules\Support\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conversation extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'ticket_id',
        'reply_id',
        'reply_type',
        'subject',
        'message',
        'ticket_conversation_id',
    ];
    protected $table = 'ticket_conversations';
    protected $appends = ['reply_type_label'];

    public function getReplyTypeLabelAttribute(): string
    {
        $reply_type = $this->reply_type == User::class ? 'User' : 'Admin';
        return $reply_type;

    }

    protected static function newFactory()
    {
        return \Modules\Support\Database\factories\ConversationFactory::new();
    }

    public function ticket(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Ticket::class);
    }

    public function reply(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo('reply');
    }
}
