<?php

namespace Modules\Support\Entities;

use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Ticket extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'creator_id',
        'creator_type',
        'department_id',
        'status',
        'priority',
        'owner_type',
        'owner_id',
        'subject'
    ];
    protected $table = 'tickets';
    protected $appends = ['status_label'];

    const STATUS_CLOSED = 0;
    const STATUS_OPEN = 1;
    const STATUS_ANSWERED = 2;
    const STATUS_USER_REPLY = 3;

    const PRIORITY_LOW = 0;
    const PRIORITY_MEDIUM = 1;
    const PRIORITY_HIGH = 2;


    public function getStatusLabelAttribute()
    {
       // switch
        switch ($this->status){
            case '0':
                return 'open';
            case '1':
                return 'closed';
            case '2':
                return 'answered';
            case '3':
                return 'user_reply';
            default:
                return 'undefined';
        }
    }
    protected static function newFactory()
    {
        return \Modules\Support\Database\factories\TicketFactory::new();
    }


    public function listTicketsByStatus(): LengthAwarePaginator
    {
        $tickets = self::query();
        $tickets->when(request('filter_status') == self::STATUS_OPEN, function ($query) {
            $query->where('creator_id', auth()->guard('admin')->user()->id)->where('status', self::STATUS_OPEN);
        });
        $tickets->when(request('filter_status') == self::STATUS_CLOSED, function ($query) {
            $query->where('creator_id', auth()->guard('admin')->user()->id)->where('status', self::STATUS_CLOSED);
        });

        $data = $tickets->latest()->paginate(15);
        return $data;
    }


    public function ListTicketsByPriority(): LengthAwarePaginator
    {
        $tickets = self::query();
        $tickets->when(request('filter_priority') == self::PRIORITY_LOW, function ($query) {
            $query->where('creator_id', auth()->guard('admin')->user()->id)->where('priority', self::PRIORITY_LOW);
        });
        $tickets->when(request('filter_priority') == self::PRIORITY_MEDIUM, function ($query) {
            $query->where('creator_id', auth()->guard('admin')->user()->id)->where('priority', self::PRIORITY_MEDIUM);
        });
        $tickets->when(request('filter_priority') == self::PRIORITY_HIGH, function ($query) {
            $query->where('creator_id', auth()->guard('admin')->user()->id)->where('priority', self::PRIORITY_HIGH);
        });

        $data = $tickets->latest()->paginate(15);
        return $data;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }

    public function creator(): MorphTo
    {
        return $this->morphTo('creator');
    }

    public function conversations(): HasMany
    {
        return $this->hasMany(Conversation::class);
    }

    public function owner()
    {
        return $this->morphTo('owner');
    }
}
