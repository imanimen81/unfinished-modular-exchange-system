<?php

namespace Modules\Support\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'status',
    ];
    protected $table = 'departments';

    protected static function newFactory()
    {
        return \Modules\Support\Database\factories\DepartmentFactory::new();
    }

    const ACTIVE = 1;
    const INACTIVE = 0;

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

    public static function getDepartment(Request $request){
        return Department::query()
            ->where('id',$request->get('department_id'))
            ->firstOrFail();
    }
}
