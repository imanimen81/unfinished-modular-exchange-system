<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Support\Http\Controllers\V2\TicketController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => '/v2/support'], function ($router) {
    $router->post('/open-ticket', [TicketController::class, 'openTicket']);
    $router->post('/close-ticket', [TicketController::class, 'closeTicket']);
    $router->get('/get-tickets', [TicketController::class, 'getTickets']);
    $router->get('/show-ticket/{id}', [TicketController::class, 'showTicket']);
    $router->get('/filter-by-status', [TicketController::class, 'ticketListStatus']);
    $router->get('/filter-by-priority', [TicketController::class, 'ticketListPriority']);
    $router->post('/filter-by-department', [TicketController::class, 'ticketListDepartments']);

    $router->post('/get-ticket-conversations', [TicketController::class, 'ticketConversation']);
    $router->post('/reply-conversation', [TicketController::class, 'replyConversation']);
    $router->post('/reply-ticket', [TicketController::class, 'replyTicket']);
});
