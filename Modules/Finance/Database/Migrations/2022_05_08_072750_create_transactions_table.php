<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_wallet_id')->nullable()->references('id')->on('user_wallets');
            $table->double('amount')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('tag')->nullable();
            $table->integer('wage')->nullable();
            $table->dateTime('accept_date')->nullable();
            $table->dateTime('expire_date')->nullable();
            $table->morphs('item');
            $table->nullableMorphs('creator');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
