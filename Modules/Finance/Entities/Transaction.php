<?php

namespace Modules\Finance\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Coin\Entities\Coin;
use Modules\Trade\Entities\Trade;
use Modules\Wallet\Entities\Wallet;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'payment_method',
        'user_wallet_id',
        'amount',
        'type',
        'tag',
        'status',
        'wage',
        'accept_date',
        'expire_date',
        'item_type',
        'item_id',
        'creator_type',
        'creator_id'
    ];
    protected $table = 'transactions';
    protected $appends = ['status_label', 'payment_method_label'];

    const BUY_TRADE = 0;
    const SELL_TRADE = 1;

    const PENDING_STATUS = 0;
    const ACCEPTED_STATUS = 1;
    const REJECTED_STATUS = 2;
    const EXPIRED_STATUS = 3;

    const WALLET_PAYMENT = 1;
    const BANK_PAYMENT = 2;
    const ADMIN_PAYMENT = 3;

    public function getPaymentMethodLabelAttribute(){
        switch ($this->payment_method){
            case self::WALLET_PAYMENT:
                return 'wallet_payment';
                break;
            case self::BANK_PAYMENT:
                return 'bank_payment';
                break;
            case self::ADMIN_PAYMENT:
                return 'admin_payment';
                break;
        }
    }

    public function getStatusLabelAttribute(){
        switch ($this->status){
            case self::PENDING_STATUS:
                return 'Pending';
            case self::ACCEPTED_STATUS:
                return 'Accepted';
            case self::REJECTED_STATUS:
                return 'Rejected';
            case self::EXPIRED_STATUS:
                return 'Expired';
        }
    }


    protected static function newFactory()
    {
        return \Modules\Finance\Database\factories\TransactionFactory::new();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function trade(){
        return $this->belongsTo(Trade::class);
    }

    public function coin(){
        return $this->belongsTo(Coin::class);
    }

    public function wallet(){
        return $this->belongsTo(Wallet::class);
    }

    public function item(){
        return $this->morphTo('item');
    }

    public function creator(){
        return $this->morphTo('creator');
    }

}
