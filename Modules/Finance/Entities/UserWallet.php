<?php

namespace Modules\Finance\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserWallet extends Model
{
    use HasFactory;

    protected $table = 'user_wallets';
    protected $fillable = [
        'creator_type',
        'creator_id',
        'owner_type',
        'owner_id',
        'amount'
    ];

    protected static function newFactory()
    {
        return \Modules\Finance\Database\factories\UserWalletFactory::new();
    }

    public function user(){
        return $this->morphOne(User::class, 'owner', 'owner_type', 'owner_id');
    }
}
