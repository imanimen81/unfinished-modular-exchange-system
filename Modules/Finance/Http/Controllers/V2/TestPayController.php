<?php

namespace Modules\Finance\Http\Controllers\V2;

use GuzzleHttp\Client;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Finance\Http\Services\PayIr;


class TestPayController extends Controller
{
    // test pay
    public function test(){
        $api = "test";
        $amount = 0;
        $redirect = 'http://localhost:8000/api/v2/finance/callback';
        $mobile = '09116014560';
        $factorNumber = '123456789';
        $description = 'test';
        $send = (new PayIr)->send($api, $amount, $redirect, $mobile, $factorNumber, $description);
        return response()->json($send);

    }

    public function verify(Request $request){
        $api = "test";
        $token = $request->get('token');
        $verify = PayIr::verify($api, $token);
        if($verify){
            return 'done';
        } else {
            return 'not done';
        }

    }


    public function callback(Request $request)
    {
        $token = $request->get('token');
        $status = $request->get('status');
        return response()->json(['status' => $status, 'token' => $token]);
    }



}
