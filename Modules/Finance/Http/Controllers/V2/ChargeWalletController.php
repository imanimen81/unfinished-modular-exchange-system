<?php

namespace Modules\Finance\Http\Controllers\V2;

use App\Http\Traits\Logs;
use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Finance\Entities\Transaction;
use Modules\Finance\Entities\UserWallet;

class ChargeWalletController extends Controller
{
    use Logs;
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get balance
    public function getWalletBalance(){
        try {
           $user = auth()->user();
          $userWallet = $user->userWallet()->sum('amount');
           return response()->json([
               'data' => [
                   'balance' => $userWallet
               ]
           ]);

        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    public function chargeWallet(Request $request){
        try {
            $user = auth()->user();
            $data = [
                'owner_id' => $user->id,
                'owner_type' => User::class,
                'creator_id' => $user->id,
                'creator_type' => User::class,
                'amount' => $request->get('amount')
            ];
            $userWallet = UserWallet::query()->create($data);
            $this->addLog($request, 'user charged wallet. Amount: '.$data['amount'], $user->id, User::class, UserWallet::class, Log::STATUS_SUCCESS);
                $walletTransaction = Transaction::query()->create([
                    'user_wallet_id' => $user->userWallet()->id,
                    'amount' => $data['amount'],
                    'status' => Transaction::PENDING_STATUS,
                    'tage' => $request->get('tag'),
                    'accept_date' => Carbon::now(),
                    'item_type' => UserWallet::class,
                    'item_id' => $user->userWallet()->id,
                    'creator_type' => User::class,
                    'creator_id' => $user->id,
                    'payment_method' => Transaction::BANK_PAYMENT
                ]);
                $this->addLog($request, 'transaction created for wallet charge. wallet_id: '.$walletTransaction['user_wallet_id'], $user->id, User::class, Transaction::class, Log::STATUS_SUCCESS);
            return response()->json([
                'message' => 'Added successfully. Waiting for your payment',
                'data' => [
                    'wallet' => $userWallet,
                    'transaction' => $walletTransaction
                ]
            ], 201);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }
    protected function getWallet(Request $request){
        return UserWallet::query()
            ->where('user_id', auth()->user()->id)
            ->where('id', $request->get('wallet_id'))
            ->firstOrFail()->id;
    }
}
