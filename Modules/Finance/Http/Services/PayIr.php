<?php

namespace Modules\Finance\Http\Services;

class PayIr
{

    function curl_post($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
        ]);
        $res = curl_exec($ch);
        curl_close($ch);

        return $res;
    }

    public function send($mobile, $amount, $redirect, $factorNumber, $description){
        $api = "test";
        $params = [
            'api' => $api,
            'amount' => $amount,
            'redirect' => $redirect,
            'factorNumber' => $factorNumber,
            'description' => $description,
            'mobile' => $mobile,
        ];
        $result = self::curl_post('https://pay.ir/pg/send', $params);
        return json_decode($result);
    }

    public function verify($api, $token){
        $params = [
            'api' => $api,
            'token' => $token,
        ];
        $result = self::curl_post('https://pay.ir/pg/verify', $params);
        return json_decode($result);
    }

}
