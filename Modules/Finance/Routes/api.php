<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Finance\Http\Controllers\V2\TestPayController;
use Modules\Finance\Http\Controllers\V2\ChargeWalletController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v2/finance'], function ($router){
   $router->get('/test-pay', [TestPayController::class, 'test']);
   $router->post('/test-verify', [TestPayController::class, 'verify']);
   $router->post('/callback', [TestPayController::class, 'callback']);

   $router->get('/user-wallet-balance', [ChargeWalletController::class, 'getWalletBalance']);
   $router->post('/charge-wallet', [ChargeWalletController::class, 'chargeWallet']);
});
