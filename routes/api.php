<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminSupportController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\AdminCoinController;
use App\Http\Controllers\Admin\AdminTradeController;
use App\Http\Controllers\Admin\UploadCoinIconController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AdminUserWalletController;
use App\Http\Controllers\Admin\AdminLogController;
use App\Http\Controllers\Admin\AdminBankAccountController;
use App\Http\Controllers\Admin\SiteSettingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v2/admin'], function ($router) {
    Route::group(['prefix' => 'auth'], function ($router) {
        $router->post('/register', [AuthController::class, 'register']);
        $router->post('/login', [AuthController::class, 'login']);
        $router->get('/logout', [AuthController::class, 'logout'])->middleware('operator');
        $router->post('/refresh', [AuthController::class, 'refresh'])->middleware('operator');
        $router->get('/profile', [AuthController::class, 'profile'])->middleware('operator');
    });
    Route::group(['prefix' => 'manage'], function ($router) {
        // users
        $router->get('/get-users', [UserController::class, 'getUsers'])->middleware('operator');
        $router->post('/add-user', [UserController::class, 'addUser'])->middleware('operator');
        $router->get('/get-users-by-status', [UserController::class, 'getUsersByStatus'])->middleware('operator');

        $router->get('/get-user', [UserController::class, 'getUserToConfirm'])->middleware('operator');
        $router->post('/confirm-user', [UserController::class, 'confirmUser'])->middleware('operator');
        $router->post('/reject-user', [UserController::class, 'rejectUser'])->middleware('operator');
        // levels
        $router->get('/get-levels', [AdminController::class, 'getLevels'])->middleware('operator');
        $router->post('/add-level', [AdminController::class, 'addLevel'])->middleware('operator');
        $router->post('/update-level', [AdminController::class, 'updateLevel'])->middleware('operator');

        // users and operators
        $router->post('/add-operator', [AdminController::class, 'addOperator'])->middleware('admin');
        $router->get('/get-operators', [AdminController::class, 'getOperators'])->middleware('admin');
        $router->post('/update-operator', [AdminController::class, 'updateOperator'])->middleware('admin');
        $router->post('/delete-operator', [AdminController::class, 'deleteOperator'])->middleware('admin');

        // site settings
        $router->get('/get-site-settings', [SiteSettingController::class, 'index'])->middleware('admin');
        $router->post('/update-site-settings', [SiteSettingController::class, 'insert'])->middleware('admin');
    });

    Route::group(['prefix' => 'support'], function ($router) {
        // departments
        $router->get('/get-departments', [AdminSupportController::class, 'getDepartments'])->middleware('operator');
        $router->post('/add-department', [AdminSupportController::class, 'addDepartment']);
        $router->post('/update-department', [AdminSupportController::class, 'updateDepartment']);
        $router->post('/delete-department', [AdminSupportController::class, 'deleteDepartment']);
        $router->get('/get-tickets', [AdminSupportController::class, 'getTickets'])->middleware('operator');
        $router->get('/get-ticket-conversation', [AdminSupportController::class, 'getConversationsByTicketId'])->middleware('operator');
        $router->post('/open-ticket', [AdminSupportController::class, 'openTicket'])->middleware('operator');
        $router->post('/close-ticket', [AdminSupportController::class, 'closeTicket'])->middleware('operator');
        $router->post('/reply-ticket', [AdminSupportController::class, 'replyTicket'])->middleware('operator');
        $router->post('/reply-conversation', [AdminSupportController::class, 'replyConversation'])->middleware('operator');
        $router->get('/show-ticket', [AdminSupportController::class, 'showTicket'])->middleware('operator');
    });

    Route::group(['prefix' => 'coins'], function ($router) {
        $router->get('/get-coins', [AdminCoinController::class, 'getCoins'])->middleware('admin');
        $router->post('/add-coin', [AdminCoinController::class, 'addCoin']);
        $router->post('/delete-coin', [AdminCoinController::class, 'deleteCoin']);
        $router->post('/upload-icon', [UploadCoinIconController::class, 'uploadIcon']);
    });

    Route::group(['prefix' => 'trades'], function ($router) {
//        $router->get('/get-trades', [AdminTradeController::class, 'getTrades']);
        $router->get('/list-trades', [AdminTradeController::class, 'listTrades'])->middleware('operator');
        $router->post('/cancel-trade', [AdminTradeController::class, 'cancelTrade']);
        $router->post('/accept-trade', [AdminTradeController::class, 'acceptTrade']);
        $router->get('/get-trades-by-type', [AdminTradeController::class, 'getTradesByType']);
        $router->get('/home', [AdminTradeController::class, 'tradeData']);
    });

    Route::group(['prefix' => 'finance'], function ($router) {
        $router->post('/charge-user-wallet', [AdminUserWalletController::class, 'chargeUserWallet']);
    });

    Route::group(['prefix' => 'logs'], function ($router){
        $router->get('/get-logs', [AdminLogController::class, 'getLogs'])->middleware('operator');
        $router->get('/get-logs-by-date', [AdminLogController::class, 'getLogsByDate'])->middleware('operator');
        $router->get('/get-users-logs', [AdminLogController::class, 'getUserLogs'])->middleware('operator');
    });

    Route::group(['prefix' => 'account'], function ($router) {
        $router->get('/get-bank-accounts', [AdminBankAccountController::class, 'getAccounts'])->middleware('operator');
        $router->post('/confirm-bank-account', [AdminBankAccountController::class, 'confirmAccount'])->middleware('operator');
        $router->post('/reject-bank-account', [AdminBankAccountController::class, 'rejectAccount'])->middleware('operator');
    });
});
