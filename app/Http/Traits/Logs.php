<?php

namespace App\Http\Traits;

use App\Models\Log;
use Illuminate\Http\Request;

trait Logs
{
    public function addLog(Request $request, $message, $user_id, $user_type, $type, $status){
        Log::query()->create([
            'user_id' => $user_id ?? null,
            'user_type' => $user_type ?? null,
            'user_agent' => $request->userAgent(),
            'ip' => $request->ip(),
            'message' => $message,
            'url' => $request->fullUrl(),
            'data' => implode(', ', $request->all()),
            'item_type' => $type ?? null,
            'item_status' => $status ?? null,
        ]);
    }
}
