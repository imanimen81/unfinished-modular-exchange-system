<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminOpenTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id' => 'required|exists:departments,id',
            'priority' => 'required|in:0,1,2',
            'subject' => 'required|string|max:255',
            'user_id' => 'required|exists:users,id',
            'message' => 'required|string|max:255',
        ];
    }
}
