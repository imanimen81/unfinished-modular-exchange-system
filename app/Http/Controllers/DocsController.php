<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocsController extends Controller
{
    /* AUTH USER */
    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/login",
     *   tags={"Authentication"},
     *   summary="user default login",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="KAMEX API DOCUMENTATION",
     *      description="",
     * )
     *
     **/
    public function defaultLogin(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/register",
     *   tags={"Authentication"},
     *   summary="user defult register",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *     @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function defaultRegister(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/check-user",
     *   tags={"Authentication"},
     *   summary="check user email or mobile",
     *
     *   @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function checkUser(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/login/otp-send",
     *   tags={"Authentication"},
     *   summary="send otp to user",
     *
     *   @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function sendOtpLogin(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/login/otp-verify",
     *   tags={"Authentication"},
     *   summary="verify otp to user",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *     @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *      @OA\Parameter(
     *      name="code_email/code_mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function verifyOtpLogin(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/register/otp-send",
     *   tags={"Authentication"},
     *   summary="send otp to user",
     *
     *   @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function registerOtp(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/register/otp-verify",
     *   tags={"Authentication"},
     *   summary="verify otp to user",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="code_email/code_mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function verifyRegisterOtp(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/profile/update-level-two",
     *   tags={"Authentication"},
     *   summary="update user profile",
     *
     *   @OA\Parameter(
     *      name="first_name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="last_name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="birth_date",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="phone",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     *      @OA\Parameter(
     *      name="national_code",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *      @OA\Parameter(
     *      name="address",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *      @OA\Parameter(
     *      name="post_code",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function updateProfile(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/profile/confirm-update",
     *   tags={"Authentication"},
     *   summary="verify user profile",
     *
     *
     *          @OA\Parameter(
     *      name="code",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function verifyUpdateProfileOtp(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/profile/upload-files",
     *   tags={"Authentication"},
     *   summary="upload user files",
     *
     *   @OA\Parameter(
     *      name="national_card_image",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *       @OA\Parameter(
     *      name="selfie_image",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function uploadFiles(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/opt/resend-sms",
     *   tags={"Authentication"},
     *   summary="resned sms otp",
     *
     *   @OA\Parameter(
     *      name="mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function resendMobileOTP(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/auth/opt/resend-email",
     *   tags={"Authentication"},
     *   summary="resend email otp",
     *
     *   @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function resendEmailOTP(){}


    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/auth/login",
     *   tags={"Admin"},
     *   summary="admin login",
     *
     *   @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *       @OA\Parameter(
     *      name="mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function adminLogin(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/auth/register",
     *   tags={"Admin"},
     *   summary="admin register",
     *
     *   @OA\Parameter(
     *      name="email/mobile",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     *       @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function adminRegister(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/manage/add-level",
     *   tags={"Admin"},
     *   summary="add level",
     *
     *   @OA\Parameter(
     *      name="title",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *    @OA\Parameter(
     *      name="level(int)",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="wage_buyer",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="wage_seller",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *    @OA\Parameter(
     *      name="max_trade",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addLevel(){}

    /**
     *
     * @OA\Get(
     ** path="/api/v2/admin/manage/get-levels",
     *   tags={"Admin"},
     *   summary="get levels",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getLevels(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/manage/update-level",
     *   tags={"Admin"},
     *   summary="update level",
     *
     *    @OA\Parameter(
     *      name="level_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *        @OA\Parameter(
     *      name="title",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *    @OA\Parameter(
     *      name="level(int)",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="wage_buyer",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="wage_seller",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *    @OA\Parameter(
     *      name="max_trade",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function updateLevel(){}


    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/manage/add-operator",
     *   tags={"Admin"},
     *   summary="add operator",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addOperator(){}

    /**
     *
     * @OA\Get(
     ** path="/api/v2/admin/manage/get-operators",
     *   tags={"Admin"},
     *   summary="add operator",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getOperators(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/manage/update-operator",
     *   tags={"Admin"},
     *   summary="update operator",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function updateOperator(){}


    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/manage/delete-operator",
     *   tags={"Admin"},
     *   summary="delete operator",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function deleteOperator(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/admin/manage/get-users",
     *   tags={"Admin"},
     *   summary="get users",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getUsers(){}

    /**
     *
     * @OA\Post   (
     ** path="/api/v2/admin/manage/add-user",
     *   tags={"Admin"},
     *   summary="add user",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addUser(){}



    /**
     *
     * @OA\Get(
     ** path="/api/v2/admin/support/get-departments",
     *   tags={"Admin"},
     *   summary="get departments",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getDepartments(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/support/add-department",
     *   tags={"Admin"},
     *   summary="add department",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addDepartment(){}


    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/support/update-department",
     *   tags={"Admin"},
     *   summary="update department",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function updateDepartment(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/support/delete-department",
     *   tags={"Admin"},
     *   summary="delete department",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function deleteDepartment(){}
    /**
     *
     * @OA\Get (
     ** path="/api/v2/admin/coins/get-coins",
     *   tags={"Admin"},
     *   summary="get coins",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getCoins(){}
    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/coins/add-coin",
     *   tags={"Admin"},
     *   summary="add coins",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addCoin(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/coins/delete-coin",
     *   tags={"Admin"},
     *   summary="delete coins",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function deleteCoin(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/coins/upload-icoin",
     *   tags={"Admin"},
     *   summary="upload icon",
     *
     *   @OA\Parameter(
     *      name="icon",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="file"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function uploadCoinIcon(){}


    /**
     *
     * @OA\Get (
     ** path="/api/v2/admin/trades/list-trades",
     *   tags={"Admin"},
     *   summary="filter_status -> 1: pending, 2: success, 3: cances, 4: expire",
     *
     *   @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function listTrades(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/trades/cancel-trade",
     *   tags={"Admin"},
     *   summary="cancel trade",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function cancelTrade(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/trades/accept-trade",
     *   tags={"Admin"},
     *   summary="Accept trade",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function acceptTrade(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/account/card/get-cards",
     *   tags={"Bank Accounts"},
     *   summary="get user cards",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getBankAccounts(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/account/card/get-sheba",
     *   tags={"Bank Accounts"},
     *   summary="get user sheba",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getShebaNumbers(){}


    /**
     *
     * @OA\Post (
     ** path="/api/v2/account/card/add-sheba",
     *   tags={"Bank Accounts"},
     *   summary="get user sheba",
     *
     *   @OA\Parameter(
     *      name="account_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="sheba_number",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addShebaNumber(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/account/card/add-card",
     *   tags={"Bank Accounts"},
     *   summary="add card",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addBankAccount(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/account/card/update-card",
     *   tags={"Bank Accounts"},
     *   summary="update card",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function updateCard(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/account/card/remove-card",
     *   tags={"Bank Accounts"},
     *   summary="remove card",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function removeCard(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/support/open-ticket",
     *   tags={"Support"},
     *   summary="open ticket",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function openTicket(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/support/close-ticket",
     *   tags={"Support"},
     *   summary="close ticket",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function closeTicket(){}

    /**
     *
     * @OA\Get   (
     ** path="/api/v2/support/get-tickets",
     *   tags={"Support"},
     *   summary="get tickets",
     *
     *   @OA\Parameter(
     *      name="trade_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getTickets(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/support/reply-ticket",
     *   tags={"Admin"},
     *   summary="reply ticket",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function replyTicket(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/account/confirm-bank-account",
     *   tags={"Admin"},
     *   summary="confirm bank account",
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function confirmBankAccount(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/account/reject-bank-account",
     *   tags={"Admin"},
     *   summary="reject bank account",
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function rejectBankAccount(){}

    /**
     *
     * @OA\Get   (
     ** path="/api/v2/support/show-ticket/{id}",
     *   tags={"Support"},
     *   summary="show ticket",
     *
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function showTicket(){}

    /**
     *
     * @OA\Post (
     ** path="/api/v2/support/get-ticket-conversations",
     *   tags={"Support"},
     *   summary="get ticket conversations(replies)",
     *
     *   @OA\Parameter(
     *      name="ticket_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getTicketConversations(){}



    /**
     *
     * @OA\Get  (
     ** path="/api/v2/support/filter-by-status",
     *   tags={"Support"},
     *   summary="filter tickets by status. 1-open, 0-closed",
     *
     *   @OA\Parameter(
     *      name="filter_status",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function filterByStatus(){}



    /**
     *
     * @OA\Get  (
     ** path="/api/v2/support/filter-by-priority",
     *   tags={"Support"},
     *   summary="filter tickets by priority. 0-low, 1-medium, 2-high",
     *
     *   @OA\Parameter(
     *      name="filter_priority",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function filterByPriority(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/support/filter-by-department",
     *   tags={"Support"},
     *   summary="filter tickets by departments",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function filterByDepartment(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/wallet/add-wallet",
     *   tags={"Crypto Wallet"},
     *   summary="add crypto wallet",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function addWallet(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/wallet/get-wallets",
     *   tags={"Crypto Wallet"},
     *   summary="get crypto wallet",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getWallets(){}

    /**
     *
     * @OA\Post   (
     ** path="/api/v2/wallet/delete-wallet",
     *   tags={"Crypto Wallet"},
     *   summary="delete crypto wallet",
     *
     *   @OA\Parameter(
     *      name="id or address",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function deleteWallet(){}
    /**
     *
     * @OA\Get  (
     ** path="/api/v2/wallet/ge-wallet-by-coin",
     *   tags={"Crypto Wallet"},
     *   summary="get crypto wallet by coin",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getWalletByCoin(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/get-trades",
     *   tags={"Trade"},
     *   summary="get user all trades",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getUserTrades(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/list-trades",
     *   tags={"Trade"},
     *   summary="filter trades. filter_status -> 0:pending, 1:succes, 2:cancelled, 3:failed",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function listTradesFilter(){}



    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/home",
     *   tags={"Trade"},
     *   summary="trade home data",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function tradeData(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/trade-history",
     *   tags={"Trade"},
     *   summary="trade history",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function tradeHistory(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/last-three-months-data",
     *   tags={"Trade"},
     *   summary="last three motnhs data",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function lastThreeMonthsData(){}



    /**
     *
     * @OA\Post  (
     ** path="/api/v2/trades/buy-trade-rial",
     *   tags={"Trade"},
     *   summary="buy trade",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function buyTrade(){}



    /**
     *
     * @OA\Post  (
     ** path="/api/v2/trades/sell-trade-rial",
     *   tags={"Trade"},
     *   summary="sell trade",
     *
     *   @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function sellTrade(){}
    /**
     *
     * @OA\Get  (
     ** path="/api/v2/admin/manage/get-users-by-status",
     *   tags={"Admin"},
     *   summary="get-users-by-status -> 0: need to verify, 1:pending, 2:verified, 3:suspended, 4:rejected",
     *
     *   @OA\Parameter(
     *      name="filter_status",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getUsersByStatus(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/admin/manage/get-user",
     *   tags={"Admin"},
     *   summary="gets user wherehas file and status pending,",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getUserToConfirmOrReject(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/manage/confirm-user",
     *   tags={"Admin"},
     *   summary="confirm user's file and update status to verified",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function confirmUser(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/manage/reject-user",
     *   tags={"Admin"},
     *   summary="reject user's file and update status to rejected",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function rejectUser(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/admin/trades/get-trades-by-type",
     *   tags={"Admin"},
     *   summary="get trades by type -> filter_type = 0: all, 1: buy, 2: sell",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getTradesByTypeAdmin(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/get-trades-by-type",
     *   tags={"Trade"},
     *   summary="get trades by type -> filter_type = 0: all, 1: buy, 2: sell",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getTradesByType(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/trades/user-properties",
     *   tags={"Trade"},
     *   summary="get user properties",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function userProperities(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/auth/profile",
     *   tags={"Authentication"},
     *   summary="get user profile",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getUserProfile(){}

    /**
     *
     * @OA\Get  (
     ** path="/api/v2/auth/logout",
     *   tags={"Authentication"},
     *   summary="logout user",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function logOut(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/auth/refresh",
     *   tags={"Authentication"},
     *   summary="refresh token",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function refreshToken(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/auth/change-password",
     *   tags={"Authentication"},
     *   summary="change password -> old_password, new_password, new_password_confirmation",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function changePassword(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/trades/get-trades-by-date",
     *   tags={"Trade"},
     *   summary="get trades data between dates -> start, end, market_type",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getTradesBetweenDates(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/support/reply-ticket",
     *   tags={"Support"},
     *   summary="reply ticket -> ticket_id, message",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function replyTicketUser(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/support/reply-conversation",
     *   tags={"Support"},
     *   summary="reply conversation -> conversatoin_id, message",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function replyConversation(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/trades/buy-trade-usd",
     *   tags={"Trade"},
     *   summary="buy trade usd",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function buyTradeUsd(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/trades/sell-trade-usd",
     *   tags={"Trade"},
     *   summary="sell trade usd",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function sellTradeUsd(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/finance/user-wallet-balance",
     *   tags={"Finance"},
     *   summary="get user wallet balance",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getWalletBalance(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/finance/charge-wallet",
     *   tags={"Finance"},
     *   summary="charge wallet",
     *
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function chargeWallet(){}

    /**
     *
     * @OA\Post(
     ** path="/api/v2/admin/finance/charge-user-wallet",
     *   tags={"Admin"},
     *   summary="Charge User Wallet",
     *
     *   @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *       @OA\Parameter(
     *      name="amount",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function chargeUserWallet(){}

    /**
     *
     * @OA\Get(
     ** path="/api/v2/admin/logs/get-logs",
     *   tags={"Admin"},
     *   summary="get logs",
     *
     *       @OA\Parameter(
     *      name="start_date",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *       @OA\Parameter(
     *      name="end_date",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getLogs(){}

    /**
     *
     * @OA\Get(
     ** path="/api/v2/admin/logs/get-logs-by-date",
     *   tags={"Admin"},
     *   summary="get logs between dates, start_date, end_date",
     *
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getLogsByDate(){}

    /**
     *
     * @OA\Get(
     ** path="/api/v2/admin/logs/get-users-logs",
     *   tags={"Admin"},
     *   summary="get logs by users type -> filter_user_type 0: admin , 1: user",
     *
     *
     * @OA\Parameter(
     *      name="filter_user_type",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getLogsByUserType(){}


    /**
     *
     * @OA\Post (
     ** path="/api/v2/auth/send-code",
     *   tags={"Authentication"},
     *   summary="send code",
     *
     *
     * @OA\Parameter(
     *      name="email",
     *      in="query",
     *
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *     @OA\Parameter(
     *      name="mobile",
     *      in="query",
     *
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function sendCode(){}

    /**
     *
     * @OA\Post (
     ** path="/api/v2/auth/auth-user",
     *   tags={"Authentication"},
     *   summary="auth user",
     *
     *
     * @OA\Parameter(
     *      name="email",
     *      in="query",
     *
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *     @OA\Parameter(
     *      name="mobile",
     *      in="query",
     *
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *         @OA\Parameter(
     *      name="code",
     *      in="query",
     *
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function authUser(){}


    /**
     *
     * @OA\Post (
     ** path="/api/v2/admin/support/reply-conversation",
     *   tags={"Support"},
     *   summary="reply conversation",
     *
     *
     * @OA\Parameter(
     *      name="message",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="conversation_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function replyConversationAdmin(){}

    /**
     *
     * @OA\Get (
     ** path="/api/v2/admin/support/get-tickets",
     *   tags={"Admin"},
     *   summary="get tickets",
     *
     *
     * @OA\Parameter(
     *      name="ticket_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getAllTickets(){}

    /**
     *
     * @OA\Get (
     ** path="/api/v2/admin/support/get-ticket-conversation",
     *   tags={"Admin"},
     *   summary="get conversations by ticket id",
     *
     *
     *
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getConvesationsByTicketId(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/support/open-ticket",
     *   tags={"Admin"},
     *   summary="open ticket",
     *
     *
     * @OA\Parameter(
     *      name="department_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     * @OA\Parameter(
     *      name="subject",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *     @OA\Parameter(
     *      name="message",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *         @OA\Parameter(
     *      name="user_id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *      name="priority",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function adminOpenTicket(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/admin/manage/get-site-settings",
     *   tags={"Admin"},
     *   summary="get site settings",
     *
     *
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getSiteSettings(){}


    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/manage/update-site-settings",
     *   tags={"Admin"},
     *   summary="update site settings",
     *
     *
     * @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     * @OA\Parameter(
     *      name="description",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *  @OA\Parameter(
     *     name="contact_numbers",
     *     in="query",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *    )
     *  ),
     *     @OA\Parameter(
     *     name="social_networks",
     *     in="query",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *   )
     * ),
     *     @OA\Parameter(
     *     name="logo",
     *      in="query",
     *     required=true,
     *     @OA\Schema(
     *     type="string"
     *  )
     * ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function updateOrCreateSiteSettings(){}

    /**
     *
     * @OA\Post  (
     ** path="/api/v2/admin/login/check-password",
     *   tags={"Authentication"},
     *   summary="check password",
     *
     *
     * @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *   ),
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function checkPassword(){}


    /**
     *
     * @OA\Get  (
     ** path="/api/v2/auth/profile/get-user-profile",
     *   tags={"Authentication"},
     *   summary="get user profile level 2",
     *
     *
     *
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *)
     **/
    public function getUserProfileLevel2(){}
}
