<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Account\Entities\BankAccount;

class AdminBankAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('operator');
    }

    // confirm user bank account
    public function confirmAccount(Request $request){
        try {
            $account = $this->getBankAccount($request);
            if ($account->status == 0) {
                $account->update([
                    'is_verified' => 1,
                    'status' => BankAccount::VERIFIED,
                ]);
                return response()->json([
                    'message' => 'confirmed successfully',
                ], 200);
            }
            if ($account->status == BankAccount::VERIFIED) {
                return response()->json([
                    'message' => 'confirmed already',
                ], 422);
            }
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    // reject user bank account
    public function rejectAccount(Request $request){
        try {
            $account = $this->getBankAccount($request);
            if ($account->is_verified !== 1) {
                $account->update([
                    'is_verified' => 2,
                    'status' => BankAccount::REJECTED,
                    'reject_reason' => $request->get('reject_reason'),
                ]);
                return response()->json([
                    'message' => 'rejected successfully',
                ], 201);
            } if ($account->is_verified == 2 && $account->status == BankAccount::REJECTED) {
                return response()->json([
                    'message' => 'rejected already',
                ], 422);
            }
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    public function getAccounts(){
        try {
            $data = BankAccount::all();
            return response()->json([
                'data' => $data,
            ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                    ]
            ], 500);
        }
    }

    protected function getBankAccount(Request $request){
        return BankAccount::query()
            ->where('id', $request->get('id'))
//            ->where('status', BankAccount::PENDING)
            ->first();
    }
}
