<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Coin\Entities\Coin;
use Modules\Coin\Http\Requests\UploadIconRequest as UploadIconRequest;

class UploadCoinIconController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function uploadIcon(UploadIconRequest $request){
        try {
            // handle file upload
            if ($request->hasFile('icon')){
                $fileNameWithExt = $request->file('icon')->getClientOriginalName();
                $fileName = pathinfo($fileNameWithExt,  PATHINFO_BASENAME);
                $extension = $request->file('icon')->getClientOriginalExtension();
                // Filename to store
                $fileNameToStore = $fileName.'_'.time().'.'.$extension;
                $path = $request->file('icon')->storeAs('public/coins/icons', $fileNameToStore);
                $coin = $this->getCoin($request);
                if ($coin){
                    $coin->update([
                        'icon' =>  'storage/coins/icons/' . $fileNameToStore,
                    ]);
                    return response()->json(['message' => 'Icon Successfully Updated','coin' => $coin], 200);
                }
                return response()->json([
                    'errors' => [
                        'message' => 'Coin not found',
                    ],
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    protected function getCoin(Request $request)
    {
        $coin = Coin::query()
            ->where('key', $request->get('coin'))
            ->orWhere('id', $request->get('coin'))
            ->firstOrFail();
        return $coin;
    }
}
