<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminOpenTicketRequest as AdminOpenTicketRequest;
use App\Models\Admin;
use App\Http\Requests\AddDepartmentRequest as AddDepartmentRequest;
use App\Http\Requests\GetDepartmentRequest as GetDepartmentRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Modules\Support\Entities\Conversation;
use Modules\Support\Entities\Department;
use Modules\Support\Entities\Ticket;

class AdminSupportController extends Controller
{
    public function __construct()
    {
        $this->middleware('operator');
    }

    public function openTicket(AdminOpenTicketRequest $request)
    {
        try {
            $data = [
                'creator_id' => auth()->guard('admin')->user()->id,
                'creator_type' => Admin::class,
                'department_id' => $request->get('department_id'),
                'status' => Ticket::STATUS_OPEN,
                'priority' => $request->get('priority'),
                'subject' => $request->get('subject'),
                'owner_id' => $request->get('user_id'),
                'owner_type' => User::class,
            ];
            $ticket = Ticket::query()->create($data);
            $conv = Conversation::query()->create([
                'ticket_id' => $ticket->id,
                'reply_id' => auth()->guard('admin')->user()->id,
                'reply_type' => Admin::class,
                'message' => $request->get('message'),
                'subject' => $request->get('subject'),
            ]);
            return response()->json([
                'message' => 'Ticket opened successfully',
                'data' => [
                    'ticket' => $ticket,
                    'ticket_conversation' => $conv,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 500);
        }
    }


    public function getTickets()
    {
        try {
            $tickets = Ticket::query()
//                ->with('conversations')
                ->paginate(15);
            return response()->json([
                'data' => $tickets,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' =>  [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    // close ticket
    public function closeTicket(Request $request)
    {
        try {
            $ticket = $this->getTicket($request);
            $ticket->status = Ticket::STATUS_CLOSED;
            $ticket->save();
            return response()->json([
                'message' => 'Ticket closed successfully',
                'data' => [
                    'ticket_id' => $ticket->id,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 500);
        }
    }

    public function getConversationsByTicketId(Request $request)
    {
        try {
            $conversations = Conversation::query()
                ->where('ticket_id', $request->get('ticket_id'))
                ->paginate(15);
            return response()->json([
                'data' => $conversations,
            ],200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' =>  [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    // add support department
    public function addDepartment(AddDepartmentRequest $request)
    {
        $data = [
          'name' => $request->get('name'),
          'status' => $request->get('status'),
        ];
        $department = Department::query()->create($data);
        return response()->json([
            'message' => 'Department added successfully',
            'data' => $department,
        ],201);
    }

    public function updateDepartment(AddDepartmentRequest $request){
        $data = [
            'name' => $request->get('name'),
            'status' => $request->get('status'),
          ];
        $department = $this->getDepartment($request);
        if ($department) {
            $department->update($data);
            return response()->json([
                'message' => 'Department updated successfully',
                'data' => $department,
            ], 201);
        } else {
            return response()->json([
                'errors' => [
                    'message' => 'Department not found',
                ],
            ], 404);
        }
    }

    public function deleteDepartment(GetDepartmentRequest $request){
        try {
            $department = $this->getDepartment($request);
            if ($department){
                $department->delete();
            } else {
                return response()->json([
                    'errors' => [
                        'message' => 'Department not found',
                    ],
                ], 404);
            }

            return response()->json([
                'message' => 'Department deleted successfully',
                'data' => $department,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Department could not be deleted',
            ]);
        }
    }

    // reply ticket
    public function replyTicket(Request $request){
        try {
            $ticket = $this->getTicket($request);
            if ($ticket){
                $ticketMessage = Conversation::query()->create([
                    'ticket_id' => $ticket->id,
                    'reply_id' => auth()->guard('admin')->user()->id,
                    'reply_type' => Admin::class,
                    'message' => $request->get('message'),
                    'subject' => $ticket->subject,
                ]);
                $ticket->update([
                   'status' => Ticket::STATUS_ANSWERED
                ]);
                return response()->json([
                    'message' => 'Ticket replied successfully',
                    'data' => [
                        'ticket_conversation' => $ticketMessage,
                    ],
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ]);
            }
    }

    // show ticket
    public function showTicket(Request $request)
    {
        try {
            $ticket = $this->getTicket($request);
            if ($ticket) {
                return response()->json([
                    'data' => [
                        'ticket' => $ticket,
                        'ticket_conversations' => $ticket->conversations,
                    ],
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 500);
        }
    }
    public function replyConversation(Request $request)
    {
        try {
            $conv = $this->getConversation($request);
            $conversation = Conversation::query()->create([
                'ticket_id' => $conv->ticket_id,
                'reply_id' => auth()->guard('admin')->user()->id,
                'reply_type' => Admin::class,
                'message' => $request->get('message'),
                'subject' => $conv->subject,
                'ticket_conversation_id' => $conv->id,
            ]);
            $ticket = $conv->ticket;
            $ticket->update([
                'status' => Ticket::STATUS_OPEN,
            ]);
            return response()->json([
                'message' => 'Ticket replied successfully',
                'data' => [
                    'ticket_conversation' => $conversation,
                ],
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }

    protected function getConversation(Request $request){
        return Conversation::query()
//            ->where('reply_id', auth()->user()->id)
            ->where('id',$request->get('conversation_id'))
            ->firstOrFail();
    }


    public function getDepartments(Request $request)
    {
       $departments = Department::all();
       return response()->json([
           'data' => $departments,
       ], 200);
    }

    protected function getDepartment(Request $request)
    {
        $department = Department::query()->find($request->get('dept_id'));
        return $department;
    }

    public function getTicket(Request $request){
        return Ticket::query()
            ->where('id', $request->get('ticket_id'))
            ->firstOrFail();
    }

    public function closeTickets(Request $request){
        try {
            $ticket = $this->getTicket($request);
            if ($ticket){
                $ticket->update([
                    'status' => Ticket::STATUS_CLOSED,
                ]);
                return response()->json([
                    'message' => 'Ticket closed successfully',
                    'data' => $ticket,
                ], 201);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ]);
            }
    }
}
