<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRequest as AddUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\Level;
use Modules\Auth\Entities\Profile;
use Modules\Auth\Entities\UserFile;
use function MongoDB\BSON\toJSON;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('operator');
    }

    // get all users
    public function getUsers(){
        $users = User::query()->paginate(15);
        return response()->json([
            'data' => $users,
        ],200);
    }

    // add user
    public function addUser(AddUserRequest $request)
    {
        $data = [
            'email' => $request->get('email'),
            'mobile' => $request->get('mobile'),
            'password' => Hash::make($request->get('password')),
//            'role' => User::ROLE_USER,
            'level_id' => Level::query()->where('level', 1)->first()->id,
            'status' => User::STATUS_NEED_TO_VERIFY,
        ];
        $user = User::query()->create($data);
        return response()->json([
            'message' => 'User Added Successfully',
            'data' => [
                'user' => $user,
                'password' => $request->get('password'),
            ],
        ], 201);
    }

    public function getUsersByStatus(Request $request){
        try {
            $users = (new User)->filterUserByStatus();
            return response()->json([
                'data' => $users,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 404);
        }
    }

    // confirm and verify user
    public function getUserToConfirm(Request $request){
        try {
            $user_id = $request->get('user_id');
            $user = $this->getUser($user_id);
            if ($user){
                $userFile = $user->file;
            }
            return response()->json([
                'data' => [
                    'user' => $user,
                ]
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 404);
        }
    }

    public function confirmUser(Request $request){
        try {
            $user_id = $request->get('user_id');
            $user = $this->getUser($user_id);
            if ($user){
                $userFile = $user->file;
            }
            $confirmFile = $user->file->update([
                'is_confirmed' => UserFile::IS_CONFIRMED,
            ]);
            $confirmUser = $user->update([
                'status' => User::STATUS_VERIFIED,
                'level_id' => 2,
            ]);
            // $profile = $user->profile->update([
            //     'status' => Profile::STATUS_VERIFIED,
            // ]);
            return response()->json([
                'message' => 'User verified successfully',
                // 'user' => $user,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 404);
        }
    }

    public function rejectUser(Request $request){
        try {
            $user_id = $request->get('user_id');
            $user = $this->getUser($user_id);
            if ($user){
                $userFile = $user->file;
            }
            $rejectFile = $user->file->update([
                'is_confirmed' => UserFile::IS_NOT_CONFIRMED,
                'reject_reason' => $request->get('reject_reason'),
            ]);
            $rejectUser = $user->update([
                'status' => User::STATUS_REJECTED,
            ]);
            return response()->json([
                'message' => 'User rejected successfully',
                'user' => $user,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 404);
        }
    }

    protected function getUser($user_id)
    {
        return User::query()
            ->where('id', $user_id)
            ->whereHas('file')
            ->where('status', User::STATUS_PENDING)
            ->firstOrFail();
    }

}
