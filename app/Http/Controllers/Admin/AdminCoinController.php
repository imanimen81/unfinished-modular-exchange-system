<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddCoinRequest as AddCoinRequest;
use Illuminate\Http\Request;
use Modules\Coin\Entities\Coin;

class AdminCoinController extends Controller
{

    // get coin list
    public function getCoins(){
        $coins = Coin::all();
        return response()->json([
            'data' => $coins,
        ], 200);
    }

    // add coin
    public function addCoin(AddCoinRequest $request){
        $data = [
            'name' => $request->get('name'),
            'key' => $request->get('key'),
            'status' => $request->get('status'),
        ];
        $coin = Coin::query()->create($data);
        return response()->json([
            'message' => 'Coin added successfully',
            'data' => $coin,
        ], 200);
    }

    // delete coin
    public function deleteCoin(Request $request){
        try {
            $coin = $this->getCoin($request);
            if ($coin) {
                $coin->delete();
                return response()->json([
                    'message' => 'Coin deleted successfully',
                ], 200);
            }
            return response()->json([
                'errors' => [
                    'message' => 'Coin not found',
                ]
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 500);
        }
    }

    protected function getCoin(Request $request){
        return Coin::query()
             ->where('id', $request->get('coin_id'))
             ->first();
    }
}
