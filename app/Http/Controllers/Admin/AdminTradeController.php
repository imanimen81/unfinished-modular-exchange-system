<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Coin\Entities\Coin;
use Modules\Trade\Entities\Trade;
use Modules\Trade\Http\Traits\TradeData;

class AdminTradeController extends Controller
{
    use TradeData;

    public function __construct()
    {
        $this->middleware('admin');
    }

    // list trades
    public function listTrades(Request $request)
    {
        return Trade::listTrades($request);
    }

    public function getTradesBytype(Request $request){

        return Trade::tradeType($request);
    }

    // cancel trade
    public function cancelTrade(Request $request){
        try{
            $trade = $this->getTrade($request);
            $trade->update([
                'status' => Trade::CANCEL_STATUS
            ]);
            return response()->json([
                'message' => 'Trade has been canceled successfully',
                'data' => [
                    'trade' => $trade,
                ]
            ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    // accept trade
    public function acceptTrade(Request $request){
        try{
            $trade = $this->getTrade($request);
            $trade->update([
                'status' => Trade::SUCCESS_STATUS
            ]);
            return response()->json([
                'message' => 'Trade has been accepted successfully',
                'data' => [
                    'trade' => $trade,
                ]
            ]);
        } catch (\Exception $e){
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 500);
        }
    }

    // main data
    public function tradeData(){

        try {
            $coins = Coin::all();
            foreach ($coins as $coin) {
                list($count, $buyPriceRial, $sellPriceRial, $countUsd, $buyPriceUsd, $sellPriceUsd,
                    $max24hours, $min24hours, $turnOverCountRial,$turnOverCountUsd, $max24hoursUsd, $min24hoursUsd, $priceChange, $priceChangeUsd) = $this->getData($coin);
                $rialData[] = [
                    'coin' => $coin->name,
                    'key' => $coin->key,
                    'icon' => $coin->icon,
                    'icon_url' => $coin->icon_url,
                    'buyPriceRial' => $buyPriceRial,
                    'sellPriceRial' => $sellPriceRial,
                    'max24Hours' => $max24hours,
                    'min24Hours' => $min24hours,
                    'volume' => $turnOverCountRial,
                    'priceChange' => '%'.$priceChange
                ];
                $usdData[] = [
                    'coin' => $coin->name,
                    'key' => $coin->key,
                    'icon' => $coin->icon,
                    'icon_url' => $coin->icon_url,
                    'buyPriceUsd' => $buyPriceUsd,
                    'sellPriceUsd' => $sellPriceUsd,
                    'max24Hours' => $max24hoursUsd,
                    'min24Hours' => $min24hoursUsd,
                    'volume' => $turnOverCountUsd,
                    'priceChange' => '%'.$priceChangeUsd
                ];
            }
            return response()->json([
                'data' => [
                    'rialMarket' => $rialData,
                    'usdMarket' => $usdData,
                    'tradeCountRial' => $count,
                    'tradeCountUsd' => $countUsd,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ], 500);
        }
    }


    /**
     * @param $coin
     * @return array
     */
    protected function getData($coin): array
    {
        $count = $this->getRialTradeCount($coin);
        $buyPriceRial = $this->getLastBuyTradePriceRial($coin->id);
        $sellPriceRial = $this->getLastSellTradePriceRial($coin->id);
        $countUsd = $this->getUsdTradeCount();
        $buyPriceUsd = $this->getLastBuyTradePriceUsd($coin->id);
        $sellPriceUsd = $this->getLastSellTradePriceUsd($coin->id);
        $max24hours = $this->maxPriceLast24HoursRial($coin->id);
        $min24hours = $this->minPriceLast24HoursRial($coin->id);
        $turnOverCountRial = $this->turnOverCountRial($coin->id);
        $turnOverCountUsd = $this->turnOverCountUsd($coin->id);
        $min24hoursUsd = $this->minPriceLast24HoursUsd($coin->id);
        $max24hoursUsd = $this->maxPriceLast24HoursUsd($coin->id);
        $priceChange = $this->priceChangeRial($coin->id);
        $priceChangeUsd = $this->priceChangeUsd($coin->id);
        return array($count, $buyPriceRial, $sellPriceRial, $countUsd, $buyPriceUsd, $sellPriceUsd, $max24hours, $min24hours, $turnOverCountRial, $turnOverCountUsd, $min24hoursUsd,$max24hoursUsd, $priceChange, $priceChangeUsd);
    }


    // get trade
    protected function getTrade(Request $request)
    {
        return Trade::query()
            ->where('id', $request->get('trade_id'))
            ->firstOrFail();
    }
}
