<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Http\Requests\AddLevelRequest as AddLevelRequest;
use App\Http\Requests\AddOperatorRequest as AddOperatorRequest;
use App\Http\Requests\UpdateLevelRequest as UpdateLevelRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Auth\Entities\Level;

class AdminController extends Controller
{

    // get all levels
    public function getLevels(){
        $levels = Level::all();
        return response()->json([
            'data' => $levels,
        ],200);
    }

    // add level
    public function addLevel(AddLevelRequest $request)
    {
        $data = [
            'title' => $request->get('title'),
            'level' => $request->get('level'),
            'wage_buyer' => $request->get('wage_buyer'),
            'wage_seller' => $request->get('wage_seller'),
            'max_trade' => $request->get('max_trade'),
        ];
        $level = Level::query()->create($data);
        return response()->json([
            'message' => 'Level Added Successfully',
            'data' => $level,
        ],201);
    }

    public function updateLevel(UpdateLevelRequest $request)
    {
        $data = [
            'title' => $request->get('title'),
            'level' => $request->get('level'),
            'wage_buyer' => $request->get('wage_buyer'),
            'wage_seller' => $request->get('wage_seller'),
            'max_trade' => $request->get('max_trade'),
        ];
        $level = $this->getLevel($request);
        if ($level) {
            $level->update($data);
            return response()->json([
                'message' => 'Level Updated Successfully',
                'data' => $level,
            ], 201);
        } else {
            return response()->json([
                'errors' => [
                    'message' => 'Department not found',
                ],
            ], 404);
        }
    }

    protected function getLevel(Request $request)
    {
        $level = Level::query()->findOrFail($request->get('level_id'));
        return $level;
    }

    // add operator
    public function addOperator(AddOperatorRequest $request){
        $password = $request->get('password');
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'mobile' => $request->get('mobile'),
            'password' => Hash::make($password),
            'role' => Admin::ROLE_OPERATOR,
            'status' => Admin::STATUS_ACTIVE,
        ];
        $operator = Admin::query()->create($data);
        return response()->json([
            'message' => 'Operator Added Successfully',
            'data' => [
                'operator' => $operator,
                'password' => $password,
            ]
        ],201);
    }

    // get all operators
    public  function getOperators()
    {
        $operators = $this->getAllOperators();
        return response()->json([
            'data' => $operators,
        ], 200);
    }

    public function updateOperator(AddOperatorRequest $request){
        $password = $request->get('password');
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'mobile' => $request->get('mobile'),
            'password' => Hash::make($password),
            'role' => Admin::ROLE_OPERATOR
        ];
        $operator = $this->getOperator($request);
        if ($operator) {
            $operator->update($data);
            return response()->json([
                'message' => 'Operator Updated Successfully',
                'data' => [
                    'operator' => $operator,
                    'password' => $password,
                ],
            ], 201);
        } else {
            return response()->json([
                'errors' => [
                    'message' => 'Department not found',
                ],
            ], 404);
        }
    }

    public function deleteOperator(Request $request){
        try {
            $operator = $this->getOperator($request);
            if ($operator) {
                $operator->delete();
                return response()->json([
                    'message' => 'Operator Deleted Successfully',
                ], 201);
            } else {
                return response()->json([
                    'errors' => [
                        'message' => 'Department not found',
                    ],
                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ],
            ], 500);
        }
    }

    protected function getOperator(Request $request)
    {
        $operator = User::query()
            ->where('role', Admin::ROLE_OPERATOR)
            ->findOrFail($request->get('operator_id'));
        return $operator;
    }
    protected function getAllOperators(){
        return User::query()
            ->where('role', Admin::ROLE_OPERATOR)
            ->get();
    }

}
