<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminChargeUserWalletRequest as AdminChargeUserWalletRequest;
use App\Http\Traits\Logs;
use App\Models\Admin;
use App\Models\Log;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Finance\Entities\Transaction;
use Modules\Finance\Entities\UserWallet;

class AdminUserWalletController extends Controller
{
    use Logs;

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function chargeUserWallet(AdminChargeUserWalletRequest $request){
        try {
            $user = auth()->guard('admin')->user();
            $data = [
              'creator_id' => $user->id,
                'creator_type' => Admin::class,
                'owner_id' => $request->get('user_id'),
                'owner_type' => User::class,
                'amount' => $request->get('amount')
            ];
            $userWallet = UserWallet::query()->create($data);
            $transaction = Transaction::query()->create([
                'user_wallet_id' => $userWallet->id,
                'amount' => $data['amount'],
                'status' => Transaction::ACCEPTED_STATUS,
                'tage' => $request->get('tag'),
                'accept_date' => Carbon::now(),
                'item_type' => UserWallet::class,
                'item_id' => $userWallet->id,
                'creator_type' => Admin::class,
                'payment_method' => Transaction::ADMIN_PAYMENT,
                'creator_id' => $user->id,
            ]);
            $this->addLog($request, 'admin charged user wallet. Amount: '.$data['amount'].'to User: '.$data['owner_id'], $user->id, Admin::class, UserWallet::class, Log::STATUS_SUCCESS);
            return response()->json([
                'message' => 'Successfully charged wallet',
                'data' => [
                    'wallet' => $userWallet,
                    'transaction' => $transaction
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage(),
                ]
            ]);
        }
    }

    protected function getUserWallet(Request $request){
        $wallet = UserWallet::query()
            ->where('owner_id', $request->get('user_id'))
            ->where('owner_type', 'App\Models\User')
            ->first();
    }
}
