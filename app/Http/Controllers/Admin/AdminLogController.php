<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Models\User;
use Illuminate\Http\Request;

class AdminLogController extends Controller
{

    // all logs
    public function getLogs()
    {
        $logs = Log::query()->paginate(15);
        return response()->json([
            'data' => $logs,
        ],200);
    }

    // get logs between dates
    public function getLogsByDate(Request $request)
    {
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');
        $logs = Log::filterByDates($startDate, $endDate);
        return response()->json([
            'data' => $logs,
        ],200);
    }

    public function getUserLogs(Request $request)
    {
        $logs = Log::filterByUserType($request);
        return response()->json([
            'data' => $logs,
        ],200);
    }
}
