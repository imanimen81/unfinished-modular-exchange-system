<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SiteSettingRequest as SiteSettingRequest;
use App\Models\SiteSetting;
use Illuminate\Http\Request;

class SiteSettingController extends Controller
{
    // get all site settings
    public function index()
    {
        $site_settings = SiteSetting::all();
        return response()->json([
            'data' => $site_settings,
        ]);
    }

    // create and update site settings
    public function insert(SiteSettingRequest $request)
    {

        $data  = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'contact_numbers' => $request->get('contact_numbers'),
            'social_networks' => $request->get('social_networks'),
        ];
        $data['logo'] = $this->uploadLogo($request);
        $site_setting = SiteSetting::query()->first();
        if ($site_setting) {
             $site_setting->update($data);
             return response()->json([
                'data' => $site_setting,
             ]);
        } else {
            $site_setting = SiteSetting::query()->create($data);
            return response()->json([
                'data' => $site_setting,
            ]);
        }
    }

    // upload logo
    protected function uploadLogo(Request $request)
    {
        // handle file upload
        if ($request->hasFile('logo')){
            $fileNameWithExt = $request->file('logo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,  PATHINFO_BASENAME);
            $extension = $request->file('logo')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('logo')->storeAs('public/settings/logo', $fileNameToStore);
            return $path;
            }
    }
}
