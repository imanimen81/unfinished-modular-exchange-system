<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest as AdminLoginRequest;
use App\Http\Traits\Logs;
use App\Models\Admin;
use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use Logs;
    // login
    public function login(AdminLoginRequest $request)
    {
        if (!$token = auth()->guard('admin')->attempt($request->all())){
            $this->addLog($request, 'failed login',  null, Admin::class, Admin::class, Log::STATUS_FAILED);
            return response()->json([
                'error' =>
                    [
                        'message' => 'Unauthorized'
                    ]
            ], 401);
        }
        $this->addLog($request, 'succeed login',  auth()->guard('admin')->user()->id, Admin::class, Admin::class, Log::STATUS_SUCCESS);
        // create token
        return response()->json($this->createNewToken($token)->original);
    }

    public function register(Request $request)
    {
        $user = Admin::query()->create(
            [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'mobile' => $request->get('mobile'),
                'password' => Hash::make($request->get('password')),
                'role' => Admin::ROLE_ADMIN,
                'status' => Admin::STATUS_ACTIVE,
            ]
        );
        if (!$token = auth()->guard('admin')->attempt($request->all())){
            $this->addLog($request, 'failed register',  null, Admin::class, Admin::class, Log::STATUS_FAILED);
            return response()->json([
                'errors' =>
                    [
                        'message' => 'Unauthorized'
                    ]
            ], 401);
        }
        $this->addLog($request, 'succeed register',  auth()->guard('admin')->user()->id, Admin::class, Admin::class, Log::STATUS_SUCCESS);
        return $this->createNewToken($token)->original;
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth()->guard('admin')->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    public function refresh(): \Illuminate\Http\JsonResponse
    {
        return $this->createNewToken(auth()->guard('admin')->refresh());
    }

    public function profile(): \Illuminate\Http\JsonResponse
    {
        return response()->json(auth()->guard('admin')->user());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'data' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60,
                'user' => auth()->guard('admin')->user()
            ]
        ]);
    }

    protected function loginAfterRegister(Request $request)
    {
        try {
            if ($token = auth()->attempt($request->all())) {
                return response()->json([
                    'token' => $this->createNewToken($token)->original
                ], 401);
            }
            else {
                return response()->json([
                    'errors' => [
                        'message' => 'Invalid Credentials'
                    ]
                ], 401);
            }

        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => $e->getMessage()
                ]
            ], 422);
        }

    }
}
