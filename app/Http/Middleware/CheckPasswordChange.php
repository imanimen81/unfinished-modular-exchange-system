<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class CheckPasswordChange
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        if ($user->updated_at <= Carbon::now()->subMonth(1)){
            return response()->json([
                'errors' =>
                    [
                        'message' => 'Please change your password'
                    ]
            ], 401);
        }
        return $next($request);
    }
}
