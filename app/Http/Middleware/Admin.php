<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        if ( Auth::guard('admin')->check()) {
            if (Auth::guard('admin')->user()->role == 0) {
                return $next($request);
            }
            return response()->json(['message' => 'You don\'t have access to this url.'], 401);

        }
        return response()->json(['message' => 'Unauthenticated.'], 401);
    }
}
