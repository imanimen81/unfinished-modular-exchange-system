<?php

namespace App\Providers;

use App\Providers\LivePrice;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LivePrice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Providers\LivePrice  $event
     * @return void
     */
    public function handle(LivePrice $event)
    {
        //
    }
}
