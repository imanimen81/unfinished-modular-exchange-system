<?php

namespace App\Listeners;

use App\Events\LiveTradeData;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Redis;
use Modules\Trade\Entities\Trade;

class LiveTradeDataListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\LiveTradeData  $event
     * @return void
     */
    public function handle(LiveTradeData $event)
    {
        $data = $event->data;
        $redis = Redis::connection();


        if($redis->ping()){
            $redis->publish('trade', json_encode($data));
            echo $data;
        }
        else{
            echo "redis is down";
        }

    }
}
