<?php

namespace App\Console\Commands;

use App\Events\LiveTradeData;
use App\Listeners\LiveTradeDataListener;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;
use Modules\Coin\Entities\Coin;
use Modules\Trade\Entities\Trade;
class TradeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:trade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'redis trade data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = Trade::all();
        event(new LiveTradeData($data));
    }
}
