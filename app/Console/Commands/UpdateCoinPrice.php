<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Coin\Entities\Coin;
use Modules\Coin\Jobs\CoinPrice as CoinPriceJob;
use Symfony\Component\Console\Output\ConsoleOutput;

class UpdateCoinPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $coins = Coin::all();
        foreach ($coins as $coin){
            dispatch(new CoinPriceJob($coin->name));
        }

        $log = new ConsoleOutput();
        $log->writeln('Price Updated Successfully!');
    }
}
