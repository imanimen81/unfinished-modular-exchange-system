<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;


    protected $appends = ['status_label', 'role_label'];
    protected $table = 'admins';
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'password',
        'role',
        'status',
        'promoted_by',
    ];

    const STATUS = [
        0 => 'Inactive',
        1 => 'Active',
    ];
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const ROLE = [
        0 => 'Admin',
        1 => 'Operator',
    ];
    const ROLE_ADMIN = 0;
    const ROLE_OPERATOR = 1;

    public function getStatusLabelAttribute(): string
    {
        $status = $this->status ? 'Active' : 'Inactive';
        return $status;
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function getRoleLabelAttribute(): string
    {
        $role = $this->role ? 'Operator' : 'Admin';
        return $role;
    }

}
