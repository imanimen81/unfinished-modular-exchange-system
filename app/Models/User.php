<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Modules\Account\Entities\BankAccount;
use Modules\Auth\Entities\Level;
use Modules\Auth\Entities\Profile;
use Modules\Auth\Entities\UserFile;
use Modules\Finance\Entities\Transaction;
use Modules\Finance\Entities\UserWallet;
use Modules\Support\Entities\Ticket;
use Modules\Trade\Entities\Trade;
use Modules\Wallet\Entities\Wallet;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */


    protected $appends = ['status_label', 'must_change_password', 'user_level'];
    protected $fillable = [
        'name',
        'email',
        'password',
        'mobile',
        'level_id',
        'role',
        'status',
        'email_verified_at',
        'change_password_date',
    ];


    const STATUS_NEED_TO_VERIFY = 0;
    const STATUS_PENDING = 1;
    const STATUS_VERIFIED = 2;
    const STATUS_SUSPENDED = 3;
    const STATUS_REJECTED = 4;

    public function getStatusLabelAttribute(){
        if($this->status == self::STATUS_NEED_TO_VERIFY){
            return 'Need to verify';
        }elseif($this->status == self::STATUS_PENDING){
            return 'Pending';
        }elseif($this->status == self::STATUS_VERIFIED){
            return 'Verified';
        }elseif($this->status == self::STATUS_SUSPENDED){
            return 'Suspended';
        }elseif($this->status == self::STATUS_REJECTED){
            return 'Rejected';
        }
    }
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    public function getMustChangePasswordAttribute(): bool
    {
        return $this->updated_at <= Carbon::now()->subMonth(1) ? true : false;
    }

    // filter user by statys
    public function filterUserByStatus(){
        $users = User::query();
        $users->when(request('filter_status') == self::STATUS_NEED_TO_VERIFY, function ($query) {
            return $query->where('status', self::STATUS_NEED_TO_VERIFY)->get();
        });
        $users->when(request('filter_status') == self::STATUS_PENDING, function ($query) {
            return $query->where('status', self::STATUS_PENDING)->get();
        });
        $users->when(request('filter_status') == self::STATUS_VERIFIED, function ($query) {
            return $query->where('status', self::STATUS_VERIFIED)->get();
        });
        $users->when(request('filter_status') == self::STATUS_SUSPENDED, function ($query) {
            return $query->where('status', self::STATUS_SUSPENDED)->get();
        });
        $users->when(request('filter_status') == self::STATUS_REJECTED, function ($query) {
            return $query->where('status', self::STATUS_REJECTED)->get();
        });
        $data = $users->paginate(15);
        return $data;
    }

    // get user level attribute
    public function getUserLevelAttribute(){
        if($this->level_id == 1){
            return 'Level One';
        }elseif($this->level_id == 2){
            return 'Level Two';
        }elseif($this->level_id == 3){
            return 'Level Three';
        }
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function file()
    {
        return $this->hasOne(UserFile::class, 'user_id', 'id');
    }

    public function level(){
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function accounts(){
        return $this->hasMany(BankAccount::class, 'user_id');
    }

    public function trades(){
        return $this->hasMany(Trade::class, 'user_id');
    }

    public function wallets(){
        return $this->hasMany(Wallet::class, 'user_id');
    }

    public function tickets(){
        return $this->hasMany(Ticket::class, 'creator_id');
    }

    public function transactions(){
        return $this->hasMany(Transaction::class, 'user_id');
    }

    public function userWallet(){
        return $this->morphMany(UserWallet::class, 'owner', 'owner_type', 'owner_id')->latest()->first();
    }

}
