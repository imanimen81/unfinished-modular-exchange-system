<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    use HasFactory;
    protected $table = 'site_settings';
    protected $appends = ['logo_url'];
    protected $fillable = [
        'name',
        'description',
        'logo',
        'contact_numbers',
        'social_networks',
    ];

    protected $casts = [
        'contact_numbers' => 'array',
        'social_networks' => 'array',
    ];

    // get logo url attribute
    public function getLogoUrlAttribute()
    {
        return asset('https://kamex.dyneemadev.com/images/' . $this->logo);
    }



}
