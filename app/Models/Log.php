<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Log extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'user_id',
        'user_type',
        'url',
        'message',
        'ip',
        'user_agent',
        'data',
        'item_type',
        'item_status',
    ];
    protected $table = 'logs';
    protected $appends = ['status_label'];

    const STATUS_SUCCESS = 1;
    const STATUS_FAILED = 2;

    // filter logs by date
    public static function filterByDates($startDate, $endDate){
        return self::query()
            ->whereBetween('created_at', [$startDate, $endDate])
            ->paginate(15);
    }

    public function getStatusLabelAttribute()
    {
        if($this->item_status == self::STATUS_FAILED){
            return 'failed';
        } elseif($this->item_status == self::STATUS_SUCCESS) {
            return 'success';
        }
    }

    public static function filterByUserType(Request $request){
        $user = self::query();
        $user->when($request->get('filter_user_type') == 0, function ($query) {
            $query->where('user_type', Admin::class);
        });
        $user->when($request->get('filter_user_type') == 1, function ($query) {
            $query->where('user_type', User::class);
        });
        $data = $user->latest()->paginate(15);
        return $data;
    }
}
